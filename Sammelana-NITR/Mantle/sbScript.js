/* -----------------------------------------------------------
				Global JavaScript functions
   ----------------------------------------------------------- */


/*  ----------------------------------------------------------
							sbType
	----------------------------------------------------------  */

var sbRes = {
	"Ok": 0,
	"PreOk": 1,
	"No": 2,
	"NoAvail": 3,
	"Small": 4,
	"Big": 5,
	"Early": 6,
	"Late": 7
};



/*  ----------------------------------------------------------
							sbStr
	----------------------------------------------------------  */

// ckecks whether the string contains any of the given chars
function sbStr_Contains(str, cnt) {
	for (var i = 0; i < cnt.length; i++) {
		if (str.indexOf(cnt.charAt(i)) >= 0) return true;
	}
	return false;
}
// checks whether the string contains only characters of given chars
function sbStr_ContainsOnly(str, cnt) {
	for (var i = 0; i < str.length; i++) {
		if (cnt.indexOf(str.charAt(i)) < 0) return false;
	}
	return true;
}
// Replaces strings with an array of keyvalue strings
function sbStr_Replace(param_str, param/*.Key,.Value*/) {
	var i, ret = param_str, len = param.length;
	for (i = 0; i < len; i++) {
		ret.replace('{' + param[i].Key + '}', param[i].Value);
	}
	return ret;
}



/*  ----------------------------------------------------------
							sbForm
	----------------------------------------------------------  */

var sbForm = {
	"VldtrDir":		"right",
	"LenMinPwd":	6,
	"InvEml":		"\"\'\n\\~`!#$%^&*()=+[]{}|:;,<>,?/",
	"InvNm":		"`~!@#$%^&*()-_=+[]{}|\\\"\':;<,>?/0123456789",
	"InvAddr":		"\'\"~!$%^?",
	"VldPhn":		"0123456789-+",
	"VldtrOkGrp":	"", //"control-group success",
	"VldtrErrGrp":	"control-group error",
	"VldtrSep":		"|",
	"VldtrDefErr":	"Error",
	"MsgBlnk":		"Blank Field|This field cannot be left blank",
	"MsgInv":		"Invalid Value|Value entered in this field is invalid",
	"KeyEnter":		13,
	"Data":			[],
	"FtTxt":		"Continue",
	"MdTxt":		"Continue",
	"LtTxt":		"Finish",
	"FtCls":		"btn-primary",
	"MdCls":		"btn-primary",
	"LtCls":		"btn-success"
};
// validate a text as per its type and type data
function sbForm_VldtTxt(txt, typ, typd) {
	var len = txt.length;
	if (len < 1) return sbRes.Small;
	var tpd = (typd === undefined) ? "" : typd;
	tpd = (tpd > 0 && tpd.charAt(0) === "#") ? $(tpd).val() : tpd;
	switch (typ) {
		case "Eml":
			if (txt.length < 6) return sbRes.No;
			if (sbStr_Contains(txt, sbForm.InvEml)) return sbRes.No;
			var pts = txt.split("@");
			if (pts.length !== 2) return sbRes.No;
			if (pts[0].length < 1 || pts[1].length < 4) return sbRes.No;
			pts = pts[1].split(".");
			if (pts.length < 2) return sbRes.No;
			for (var i = 0; i < pts.length - 1; i++) {
				if (pts[i].length === 0) return sbRes.No;
			}
			if (pts[pts.length - 1].length < 2) return sbRes.No;
			break;
		case "Pwd":
			if (txt.length < sbForm.LenMinPwd) return sbRes.No;
			break;
		case "Nm":
			if (sbStr_Contains(txt, sbForm.InvNm)) return sbRes.No;
			break;
		case "Gndr":
			alert(txt);
			if (txt !== "Male" && txt !== "Female") return sbRes.No;
			break;
		case "Date":
		case "Time":
		case "DtTm":
			try { new Date(txt); }
			catch (e) { return sbRes.No; }
			break;
		case "Addr":
			if (sbStr_Contains(txt, sbForm.InvAddr)) return sbRes.No;
			break;
		case "Phn":
			if (txt.length > 16 || !sbStr_ContainsOnly(txt, sbForm.VldPhn)) return sbRes.No;
			break;
		case "Num":
			if (parseFloat(txt).toString() === "NaN") return sbRes.No;
			break;
		case "Int":
			if (parseInt(txt).toString() === "NaN") return sbRes.No;
			break;
		case "Mtch":
			if (txt !== tpd) return sbRes.No;
			break;
	}
	return sbRes.Ok;
}
// report some text on a field's validator
function sbForm_VldtrReport(fld, txt) {
	var sep = txt.indexOf(sbForm.VldtrSep);
	var ttl, cnt;
	if (sep < 0) {
		ttl = sbForm.VldtrDefErr;
		cnt = txt;
	}
	else {
		ttl = txt.substr(0, sep);
		cnt = txt.substr(sep+1, txt.length-sep-1);
	}
	sep = fld.lastIndexOf(":");
	if (sep > 0) {
		fld = fld.substr(0, sep) + ":first";
	}
	$(fld).popover("destroy");
	$(fld).popover({
		trigger: "manual",
		placement: sbForm.VldtrDir,
		title: ttl,
		content: cnt
	});
	$(fld).popover("show");
}
// hide a previously reported field's validator
function sbForm_VldtrHide(fld) {
	var sep = fld.lastIndexOf(":");
	if (sep > 0) {
		fld = fld.substr(0, sep) + ":first";
	}
	$(fld).popover("destroy");
}
// validate a field and accordingly report on its validator
function sbForm_VldtFld(fld, typ, typd, msg_blnk, msg_inv) {
	var txt = $(fld).val();
	var vld = false, vn = sbForm_VldtTxt((typeof(txt) === "undefined") ? "" : txt, typ, typd);
	switch (vn) {
		case sbRes.Ok:
			sbForm_VldtrHide(fld);
			break;
		case sbRes.Small:
			if (msg_blnk !== undefined && msg_blnk !== "") sbForm_VldtrReport(fld, msg_blnk);
			break;
		case sbRes.No:
			if (msg_inv !== undefined && msg_inv !== "") sbForm_VldtrReport(fld, msg_inv);
			break;
	}
	if (vn === sbRes.Ok || (vn === sbRes.Small && msg_blnk === "") || (vn === sbRes.No && msg_inv === "")) vld = true;
	if (vld) $(fld).parent().removeClass(sbForm.VldtrErrGrp).addClass(sbForm.VldtrOkGrp);
	else $(fld).parent().removeClass(sbForm.VldtrOkGrp).addClass(sbForm.VldtrErrGrp);
	return vld;
}
// validate a field with default msgs
function sbForm_VldtFldAuto(fld, typ, typd) {
	return sbVldtFld(fld, typ, typd, sbForm.MsgBlnk, sbForm.MsgInv);
}
// validate a field with default blank msg
function sbForm_VldtFldAutoBlnk(fld, typ, typd) {
	return sbVldtFld(fld, typ, typd, sbForm.MsgBlnk, "");
}
// validate a field with default invalid msg
function sbForm_VldtFldAutoInv(fld, typ, typd) {
	return sbVldtFld(fld, typ, typd, vldtr, "", sbForm.MsgInv);
}
// validate a particular field from form information object
function sbForm_VldtFldInf(fld) {
	var fVld = false;
	switch (fld.Mthd) {
		case undefined:
		case "":
			fld.Vld = sbForm_VldtFld(fld.Nm, fld.Typ, fld.Typd, fld.MsgBlnk, fld.MsgInv);
			break;
		case "Auto":
			fld.Vld = sbForm_VldtFldAuto(fld.Nm, fld.Typ, fld.Typd);
			break;
		case "AutoBlnk":
			fld.Vld = sbForm_VldtFldAutoBlnk(fld.Nm, fld.Typ, fld.Typd);
			break;
		case "AutoInv":
			fld.Vld = sbForm_VldtFldAutoInv(fld.Nm, fld.Typ, fld.Typd);
	}
	if (fld.Vld && fld.Fn !== undefined) fVld = fld.Fn();
	if (fld.Vld && !fVld && fld.MsgFn !== undefined) sbForm_VldtrReport(fld.Nm, fld.MsgFn);
	fld.Vld &= fVld;
	return fld.Vld;
}
// validate a particular block from form information object (bool act = actively)
function sbForm_VldtBlkInf(blk, act) {
	blk.Vld = true;
	var i=0, len = blk.Fld.length;
	for (i = 0; i < len; i++) {
		if (act) blk.Vld &= sbForm_VldtFldInf(blk.Fld[i]);
		else blk.Vld &= blk.Fld[i].Vld;
	}
	return blk.Vld;
}
// validate a form from form information object (bool act = actively)
function sbForm_VldtFrmInf(frm, act) {
	frm.Vld = true;
	var i = 0, len = frm.Blk.length;
	for (i = 0; i < len; i++) {
		if (act) frm.Vld &= sbForm_VldtBlkInf(frm.Blk[i], true);
		else frm.Vld &= frm.Blk[i].Vld;
	}
	return frm.Vld;
}
// change to another block in form
function sbForm_ChngBlk(dir) {
	var vld = false;
	if(dir > 0) {
		vld = sbForm_VldtBlkInf(sbForm.Data.Blk[sbForm.Data.BlkNow], true);
		if(!vld) return false;
		if(sbForm.Data.BlkNow === sbForm.Data.Blk.length - 1) {
			vld = sbForm_VldtFrmInf(sbForm.Data, false);
			if(vld) return true;
			for(var i = 0; i < sbForm.Data.Blk.length; i++) {
				if(!sbForm.Data.Blk[i].Vld) break;
			}
			if(i < sbForm.Data.Blk.length) sbForm_ChngBlk(i - sbForm.Data.BlkNow);
			return false;
		}
	}
	var stp = sbForm.Data.BlkNow + dir;
	if(stp < 0) return false;
	$(sbForm.Data.Blk[sbForm.Data.BlkNow].Nm).hide();
	$(sbForm.Data.Blk[stp].Nm).show();
	sbForm.Data.BlkNow = stp;
	$(sbForm.Data.Blk[stp].Fld[0].Nm).focus();
	if (stp === sbForm.Data.Blk.length - 1) {
		if(sbForm.Data.Sbmt.LtTxt === undefined) $(sbForm.Data.Sbmt.Nm).val(sbForm.LtTxt);
		else $(sbForm.Data.Sbmt.Nm).val(sbForm.Data.Sbmt.LtTxt);
		if (sbForm.Data.Sbmt.LtCls === undefined) $(sbForm.Data.Sbmt.Nm).removeClass("btn-primary").addClass("btn-success");
		else $(sbForm.Data.Sbmt.Nm).removeClass(sbForm.FtCls + " " + sbForm.MdCls).addClass(sbForm.LtCls);
	}
	else if (stp === 0) {
		if(sbForm.Data.Sbmt.FtTxt === undefined) $(sbForm.Data.Sbmt.Nm).val(sbForm.FtTxt);
		else $(sbForm.Data.Sbmt.Nm).val(sbForm.Data.Sbmt.FtTxt);
		if (sbForm.Data.Sbmt.FtCls === undefined) $(sbForm.Data.Sbmt.Nm).removeClass("btn-success").addClass("btn-primary");
		else $(sbForm.Data.Sbmt.Nm).removeClass(sbForm.MdCls + " " + sbForm.LtCls).addClass(sbForm.FtCls);
	}
	else {
		if(sbForm.Data.Sbmt.MdTxt === undefined) $(sbForm.Data.Sbmt.Nm).val(sbForm.MdTxt);
		else $(sbForm.Data.Sbmt.Nm).val(sbForm.Data.Sbmt.MdTxt);
		if (sbForm.Data.Sbmt.MdCls === undefined) $(sbForm.Data.Sbmt.Nm).removeClass("btn-success").addClass("btn-primary");
		else $(sbForm.Data.Sbmt.Nm).removeClass(sbForm.FtCls + " " + sbForm.LtCls).addClass(sbForm.MdCls);
	}
	return true;
}
// Add validation to a form using form information
function sbForm_AddVldtn(frm) {
	var i = 0, j = 0;
	sbForm.Data = frm;
	sbForm.Data.Vld = false;
	for (i = 0; i < sbForm.Data.Blk.length; i++) {
		sbForm.Data.Blk[i].Vld = false;
		$(sbForm.Data.Blk[i].Nm).hide();
		for (j = 0; j < sbForm.Data.Blk[i].Fld.length; j++) {
			sbForm.Data.Blk[i].Fld[j].Vld = false;
			$(sbForm.Data.Blk[i].Fld[j].Nm).focusout({ "BlkNo": i, "FldNo": j }, function (event) {
				sbForm_VldtFldInf(sbForm.Data.Blk[event.data.BlkNo].Fld[event.data.FldNo]);
			});
		}
	}
	sbForm.Data.BlkNow = 0;
	$(sbForm.Data.Sbmt.Nm).click(function () {
		return true;
	});
	if (sbForm.Data.Prev !== undefined) {
		$(sbForm.Data.Prev.Nm).click(function () {
			sbForm_ChngBlk(-1);
			return true;
		});
	}
	$(sbForm.Data.Nm).submit(function () {
		sbForm_ChngBlk(1);
		return sbForm.Data.Vld;
	});
	sbForm_ChngBlk(0);
}

/*  ----------------------------------------------------------
							sbHtml
	----------------------------------------------------------  */

var sbHtml = {
	"DfltMime":		"application/octet-stream",
	"MsgStartKey":	"JSON_SOM",
	"MsgEndKey":	"JSON_EOM",
	"MsgCntntTyp":	"text/plain",
	"MsgUrl":		""
};
function sbHtml_SetCookie(cookie, value, expDays) {
	$.cookie(cookie, value, {expires:expDays});
}
function sbHtml_SendRawMsg(msg) {
	$.get(sbHtml.MsgUrl, msg);
}
function sbHtml_SendFile() {
}
function sbHtml_SendMsg(data) {
	msg = data;
	msg["MsgStartKey"] = sbHtml.MsgStartKey;
	msg["MsgEndKey"] = sbHtml.MsgEndKey;
	$.get(sbHtml.MsgUrl, msg);
}
function sbHtml_RecvMsg() {
}

