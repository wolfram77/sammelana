﻿<%@ Page Title="Sammelana" Language="C#" MasterPageFile="/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sammelana_NITR.Pages.Default" %>
<asp:Content ID="Dflt_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Dflt_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<div class="hero-unit">
		<h2>Manage Conferences, the easy way</h2>
		<br />
		<p>
			Powerful paper submission, review and reporting system, with conference planning, role assignment, online discussions, progress tables, 
			and many more features for management of a conference
		</p>
		<br />
		<a href="/Pages/Cmn/AbtUs.aspx" class="btn btn-large btn-success">Learn more</a>
	</div>
	<ul class="thumbnails">
		<li class="span4">
			<div class="thumbnail sb-pad sb-mid">
				<div><img src="/Base/Img/Dflt/new_conf.jpg" alt="New Conference" class="sb-box img-rounded" style="height: 200px;" /></div>
				<br />
				<h4>Create a Conference</h4>
				<br />
				<p class="sb-txt-med">
					Creating a conference is just as easy as signing up to a website. Get your conference ready in no time, alongwith with new features to help
					you increase the quality of your conference with much less effort.
				</p>
				<br />
				<a href="/Pages/Cnf/View.aspx" class="btn btn-primary">Create Conference</a>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail sb-pad sb-mid">
				<div><img src="/Base/Img/Dflt/find_conf.jpg" alt="Find Conference" class="sb-box img-rounded" style="height: 200px;" /></div>
				<br />
				<h4>Browse Conferences</h4>
				<br />
				<p class="sb-txt-med">
					Browse or find conferences that are ongoing, or to be held recently. Join conferences and contribute to it by being a part of the program committee, or by
					submitting paper(s) to the conference as an author.
				</p>
				<br />
				<a href="/Pages/Cnf/View.aspx" class="btn btn-primary">Goto Conferences</a>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail sb-pad sb-mid">
				<div><img src="/Base/Img/Dflt/goto_faq.png" alt="Goto FAQ" class="sb-box img-rounded" style="height: 200px;" /></div>
				<br />
				<h4>Have Questions?</h4>
				<br />
				<p class="sb-txt-med">
					If you have any questions on how to get started, create conference, submit paper or any related question, visit the FAQ. It contains answers to
					many general quetions and helps you understand how to get started.
				</p>
				<br />
				<a href="/Pages/Cmn/Faq.aspx" class="btn btn-primary">Checkout FAQ</a>
			</div>
		</li>
	</ul>
	<div class="thumbnail sb-pad sb-mid">
		<p class="sb-hdr-med">
			This website is still under development. We would love to recieve suggestions from you regarding the features to be put in here.
		</p>
		<br />
		<a href="/Pages/Cmn/Sgst.aspx" class="btn">Provide Suggestion</a>
	</div>
</asp:Content>
