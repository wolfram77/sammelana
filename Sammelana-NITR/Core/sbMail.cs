﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net.Mail;

namespace Sammelana_NITR.Core
{
	public class sbMail
	{
		// global settings for email activities
		// the email server used by sammelana
		public static string Srvr = ConfigurationManager.AppSettings["Mail_Srvr"];
		// the email address used by sammelana
		public static string Addr = ConfigurationManager.AppSettings["Mail_Addr"];

		// Functions
		// send a normal text message to an email (from sammelana email)
		public static int Send(string dst_eml, string subject, string body)
		{
			// initialize a mailmessage object with the contents of the email (to be sent)
			MailMessage mlMsg = new MailMessage(Addr, dst_eml, subject, body);
			// initialize an smtpclient object with the address of sammelana mail server
			SmtpClient mlSrv = new SmtpClient(Srvr);
			// try to send the email through sammelana email server
			try
			{
				mlSrv.Send(mlMsg);
			}
			// if failed, return -1 indicating send failure
			catch (Exception)
			{
				return -1;
			}
			// success (status = 0)
			return 0;
		}
		// gives the email verification link for provided action id
		public static string FetchCnfLink(Guid AId)
		{
			return sbServer.Addr + "/Pages/Users/EmailVerify.aspx?md=vrml&cd=" + AId;
		}
	}
}