﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbStore
	{
		// global variables
		// log data path
		public static string Log_WebPath = ConfigurationManager.AppSettings["Store_Log"];
		public static string Log_PhyPath = HttpContext.Current.Server.MapPath(Log_WebPath);
		// user data path
		public static string Usr_WebPath = ConfigurationManager.AppSettings["Store_Usr"];
		public static string Usr_PhyPath = HttpContext.Current.Server.MapPath(Usr_WebPath);
		// conference data path
		public static string Cnf_WebPath = ConfigurationManager.AppSettings["Store_Cnf"];
		public static string Cnf_PhyPath = HttpContext.Current.Server.MapPath(Cnf_WebPath);
		// paper data path
		public static string Ppr_WebPath = ConfigurationManager.AppSettings["Store_Ppr"];
		public static string Ppr_PhyPath = HttpContext.Current.Server.MapPath(Ppr_WebPath);

		// Functions
		// get store filename
		public static string GetFileName(string prfx, int Id, string ext)
		{
			return prfx + Id + Path.GetExtension(ext);
		}
		// get full path of file
		public static string GetFullPath(string typ, string filename, bool web_path)
		{
			string ret = "";

			if (web_path)
			{
				switch (typ.ToLower())
				{
					case "log":
						ret = Log_WebPath;
						break;
					case "usr":
						ret = Usr_WebPath;
						break;
					case "cnf":
						ret = Cnf_WebPath;
						break;
					case "ppr":
						ret = Ppr_WebPath;
						break;
				}
				if(ret != "") ret += filename;
			}
			else
			{
				switch (typ.ToLower())
				{
					case "log":
						ret = Log_WebPath;
						break;
					case "usr":
						ret = Usr_WebPath;
						break;
					case "cnf":
						ret = Cnf_WebPath;
						break;
					case "ppr":
						ret = Ppr_WebPath;
						break;
				}
				if(ret != "") ret = Path.Combine(ret, filename);
			}
			return ret;
		}
		// write file to store (returns filename)
		public static string SaveFile(string path, string prfx, int id, string ext, byte[] data)
		{
			string fn = prfx + id + Path.GetExtension(ext);
			File.WriteAllBytes(Path.Combine(path, fn), data);
			return fn;
		}
		// 
	}
}