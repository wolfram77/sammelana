﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace Sammelana_NITR.Core
{
	public class sbLog
	{
		// global constants
		// the filename of the global log file
		public static string GlobalName = "Log_Global.log";
		// the filename and path of the global log file
		public static string Global = Path.Combine(sbStore.Log_PhyPath, GlobalName);

		// Functions
		// write text to log file
		public static void Write(string logText, params object[] logParams)
		{
			// initialize a new streamwriter object to open the logfile
			StreamWriter strm = new StreamWriter(Global, true);
			// write data to the stream
			strm.Write(logText, logParams);
			// flush the stream to cause the log text to be directly written to disk (without waiting for sufficient data)
			strm.Flush();
			// dispose the stream writer to close the file (to prevent loss of logs)
			strm.Dispose();
		}
		// write text line to log file
		public static void WriteLine(string logText, params object[] logParams)
		{
			// initialize a new streamwriter object to open the logfile
			StreamWriter strm = new StreamWriter(Global, true);
			// write data to the stream
			strm.WriteLine(logText, logParams);
			// flush the stream to cause the log text to be directly written to disk (without waiting for sufficient data)
			strm.Flush();
			// dispose the stream writer to close the file (to prevent loss of logs)
			strm.Dispose();
		}
		// start a block in the log file (with block title and write time)
		public static void StartBlock(string blockText, params object[] blockParams)
		{
			// initialize a new streamwriter object to open the logfile
			StreamWriter strm = new StreamWriter(Global, true);
			// disable autoflush to prevent unrequired flushing of data to disk (before entire data is added)
			strm.AutoFlush = false;
			// write the block data
			strm.WriteLine("---------------------------------------------------------------");
			strm.WriteLine("---------------------------------------------------------------");
			strm.WriteLine(blockText, blockParams);
			strm.Write("\t\t");
			strm.WriteLine("Time: {0}", DateTime.Now);
			strm.WriteLine("---------------------------------------------------------------\n");
			// flush the stream to cause the log text to be directly written to disk (without waiting for sufficient data)
			strm.Flush();
			// dispose the stream writer to close the file (to prevent loss of logs)
			strm.Dispose();
		}
		// end a block in the log file (with block title and write time)
		public static void EndBlock(string blockText, params object[] blockParams)
		{
			// initialize a new streamwriter object to open the logfile
			StreamWriter strm = new StreamWriter(Global, true);
			// disable autoflush to prevent unrequired flushing of data to disk (before entire data is added)
			strm.AutoFlush = false;
			// write the block data
			strm.WriteLine("---------------------------------------------------------------");
			strm.WriteLine(blockText, blockParams);
			strm.Write("\t\t");
			strm.WriteLine("Time: {0}", DateTime.Now);
			strm.WriteLine("---------------------------------------------------------------");
			strm.WriteLine("---------------------------------------------------------------\n\n\n");
			// flush the stream to cause the log text to be directly written to disk (without waiting for sufficient data)
			strm.Flush();
			// dispose the stream writer to close the file (to prevent loss of logs)
			strm.Dispose();
		}
	}
}