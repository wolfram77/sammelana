﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	/*
	 * Command
	 * -------
	 * 
	 * Cls:			Command Class
	 * Nm:			Command Name (Function / Property) called
	 * Fn:			true/false (is function or property)
	 * Ret:			true/false (display or not)
	 * Dst:			destination variable
	 * Prm:			Parameters (array / keyval)
	 * Lvl:			Execution level of command (decides whether fn. to be excuted or not)
	 * 
	 */
	public class sbCmd
	{
		// global variables
		// execution level of classes
		public static sbKeyVals ExecLvl = new sbKeyVals() { { "sbAct", 100 }, { "sbCnf", 100 }, { "sbDb", 100 }, { "sbForm", 100 }, { "sbHtml", 100 }, { "sbIds", 100 }, { "sbKv", 100 }, { "sbLog", 100 }, { "sbLog", 100 }, { "sbMail", 100 }, { "sbPpr", 100 }, { "sbServer", 100 }, { "sbSgt", 100 }, { "sbStore", 100 }, { "sbStr", 100 }, { "sbType", 100 }, { "sbUsr", 100 } };

		public static sbKeyVals GetCmd(string cmd_str)
		{
			int i, len;
			string cmd;
			sbKeyVals ret;
			List<string> ls;
			object[] oa;

			ret = new sbKeyVals() { { "Cls", "" }, { "Fn", false }, { "Ret", false }, { "Dst", "" }, { "Prm", null }, { "Lvl", 0 } };
			cmd = string.Copy(cmd_str);
			cmd = cmd.Trim();
			i = cmd.IndexOf("return");
			if (i == 0)
			{
				ret["Ret"] = true;
				cmd = cmd.Trim();
			}
			i = cmd.IndexOf('=');
			if (i > 0)
			{
				ret["Dst"] = cmd.Substring(0, i).Trim();
				cmd = cmd.Substring(i + 1).Trim();
			}
			i = cmd.IndexOf('.');
			if (i > 0)
			{
				ret["Cls"] = cmd.Substring(0, i).Trim();
				cmd = cmd.Substring(i + 1).Trim();
			}
			i = cmd.IndexOf('(');
			if (i < 0) ret["Nm"] = cmd;
			else
			{
				ret["Nm"] = cmd.Substring(0, i).Trim();
				ret["Fn"] = true;
				cmd = cmd.Substring(i).Trim();
				i = cmd.LastIndexOf(')');
				if (i < 0) return null;
				cmd = cmd.Substring(0, i).Trim();
				ls = sbStr.Split(cmd, ",");
				len = ls.Count;
				oa = new object[len];
				for (i = 0; i < len; i++)
					oa[i] = sbStr.ToObject(ls[i]);
				ret["Prm"] = oa;
			}
			return ret;
		}
	}
}