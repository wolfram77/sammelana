﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbAct
	{
		// clean all outdated / expired actions from the "Act" (action) table
		// otherwise action table may grow too much and contain a lot of meaningless data
		public static int CleanOudated(sbDb db)
		{
			// cleans all expired actions
			return db.RemoveRows("Act", "ExTm >= @Tm", "@Tm", DateTime.Now);
		}
		// add an action to the action table, return the action id, if successful
		public static Guid AddNew(sbDb db, int EId, string AcTp, int ExpHrs)
		{
			int ret;

			sbKeyVals Act = new sbKeyVals();
			Act["AId"] = Guid.NewGuid();
			Act["AcTp"] = AcTp;
			Act["EId"] = EId;
			Act["IsTm"] = DateTime.Now;
			Act["ExTm"] = DateTime.Now.AddHours(ExpHrs);
			ret = db.AddRow("Act", Act);
			if (ret == 0) return (Guid)Act["AId"];
			return Guid.Empty;
		}
		// get an action from its action id, expired actions are checked and removed
		public static sbKeyVals Fetch(sbDb db, Guid AId)
		{
			sbKeyVals Act = db.GetRow("Act", "*", "AId = @AId", "@AId", AId);
			if (Act == null) return null;
			if ((DateTime)Act["ExTm"] <= DateTime.Now)
			{
				db.RemoveRows("Act", "AId = @AId", "@AId", AId);
				return null;
			}
			return Act;
		}
		// remove a particular action from the action table
		public static int Remove(sbDb db, Guid AId)
		{
			return db.RemoveRows("Act", "AId = @AId", "@AId", AId);
		}
		// get the user id related to a particular action
		public static int FetchEId(sbDb db, Guid AId, string AcTp)
		{
			sbKeyVals Act;

			Act = Fetch(db, AId);
			if (Act == null) return -1;
			if ((string)Act["AcTp"] != AcTp) return -1;
			return (int)Act["EId"];
		}
	}
}
