﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbDb
	{
		// global database settings
		// connection string to the sammelana database
		public static string Sammelana = ConfigurationManager.ConnectionStrings["Db_Sammelana"].ConnectionString;

		// Data members
		public SqlConnection dbConn;
		// no. of rows affected in last operation
		public int RowsAffected;

		// Functions
		// Constructor - connect to sammelana database
		public sbDb()
		{
			dbConn = null;
			Reconnect();
		}
		// Reconnect to sammelana database
		public void Reconnect()
		{
			if (dbConn != null) dbConn.Close();
			dbConn = new SqlConnection(Sammelana);
			dbConn.Open();
		}
		// Disconnect from the database (must be executed before ending a page operation)
		public void Disconnect()
		{
			if (dbConn != null) dbConn.Close();
			dbConn = null;
		}
		// Execute a custom command on database
		public List<sbKeyVals> CstmCmd(string cmd)
		{
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			return sbKv.ToLstKeyVals(dbCmd.ExecuteReader(), true);
		}
		// Get a particular value from the database
		public object GetValue(string tbl, string getFld, string srchCrt)
		{
			string cmd = "SELECT " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0)? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			return (dbCmd.ExecuteScalar());
		}
		// get a particular value from the database (with keyvals search parameters)
		public object GetValue(string tbl, string getFld, string srchCrt, sbKeyVals srchParams)
		{
			sbKeyVal Row;

			string cmd = "SELECT " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			if (srchParams != null)
			{
				for (int i = 0; i < srchParams.Count; i++)
				{
					Row = srchParams.ElementAt(i);
					dbCmd.Parameters.AddWithValue(Row.Key, Row.Value);
				}
			}
			return (dbCmd.ExecuteScalar());
		}
		// get a particular value from the database (with multi params search parameters)
		public object GetValue(string tbl, string getFld, string srchCrt, params object[] srchParams)
		{
			string cmd = "SELECT " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < srchParams.Length; i += 2)
			{
				dbCmd.Parameters.AddWithValue((string)srchParams[i], srchParams[i + 1]);
			}
			return (dbCmd.ExecuteScalar());
		}
		// get the result set from the database
		public List<sbKeyVals> GetRows(string tbl, string getFld, string srchCrt)
		{
			string cmd = "SELECT " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			return sbKv.ToLstKeyVals(dbCmd.ExecuteReader(), true);
		}
		// get the result set from the database (with keyvals search parameters)
		public List<sbKeyVals> GetRows(string tbl, string getFld, string srchCrt, sbKeyVals srchParams)
		{
			sbKeyVal Row;

			string cmd = "SELECT " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			if (srchParams != null)
			{
				for (int i = 0; i < srchParams.Count; i++)
				{
					Row = srchParams.ElementAt(i);
					dbCmd.Parameters.AddWithValue(Row.Key, Row.Value);
				}
			}
			return sbKv.ToLstKeyVals(dbCmd.ExecuteReader(), true);
		}
		// get the result set from the database (with multi params search parameters)
		public List<sbKeyVals> GetRows(string tbl, string getFld, string srchCrt, params object[] srchParams)
		{
			string cmd = "SELECT " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < srchParams.Length; i += 2)
			{
				dbCmd.Parameters.AddWithValue((string)srchParams[i], srchParams[i + 1]);
			}
			return sbKv.ToLstKeyVals(dbCmd.ExecuteReader(), true);
		}
		// get a single row from the database
		public sbKeyVals GetRow(string tbl, string getFld, string srchCrt)
		{
			string cmd = "SELECT TOP 1 " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			return sbKv.ToKeyVals(dbCmd.ExecuteReader());
		}
		// get a single row from the database (with keyvals search parameters)
		public sbKeyVals GetRow(string tbl, string getFld, string srchCrt, sbKeyVals srchParams)
		{
			sbKeyVal Row;

			string cmd = "SELECT TOP 1 " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < srchParams.Count; i++)
			{
				Row = srchParams.ElementAt(i);
				dbCmd.Parameters.AddWithValue(Row.Key, Row.Value);
			}
			return sbKv.ToKeyVals(dbCmd.ExecuteReader());
		}
		// get a single row from the database (with multi params search parameters)
		public sbKeyVals GetRow(string tbl, string getFld, string srchCrt, params object[] srchParams)
		{
			string cmd = "SELECT TOP 1 " + getFld + "\nFROM " + tbl + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < srchParams.Length; i += 2)
			{
				dbCmd.Parameters.AddWithValue((string)srchParams[i], srchParams[i + 1]);
			}
			return sbKv.ToKeyVals(dbCmd.ExecuteReader());
		}
		// set a set of values in a database
		public int SetValues(string tbl, string updSet, string srchCrt)
		{
			string cmd = "UPDATE " + tbl + "\nSET " + updSet + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0)? 0 : -1;
		}
		// set a set of values in a database (with keyvals update as well as search parameters)
		public int SetValues(string tbl, string updCrt, string srchCrt, sbKeyVals srchParams)
		{
			sbKeyVal Row;

			string cmd = "UPDATE " + tbl + "\nSET " + updCrt + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < srchParams.Count; i++)
			{
				Row = srchParams.ElementAt(i);
				dbCmd.Parameters.AddWithValue(Row.Key, Row.Value);
			}
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
		// set a set of values in a database (with multi params update as well as search parameters)
		public int SetValues(string tbl, string updCrt, string srchCrt, params object[] srchParams)
		{
			string cmd = "UPDATE " + tbl + "\nSET " + updCrt + ((srchCrt == null || srchCrt.Length == 0) ? "" : "\nWHERE " + srchCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < srchParams.Length; i += 2)
			{
				dbCmd.Parameters.AddWithValue((string)srchParams[i], srchParams[i + 1]);
			}
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
		// add (insert) a row of data to a database (from keyvals)
		public int AddRow(string tbl, sbKeyVals insrtVal)
		{
			int i, Last;
			sbKeyVal Row;
			StringBuilder Params = new StringBuilder();
			StringBuilder Values = new StringBuilder();

			Last = insrtVal.Count - 1;
			for (i = 0; i <= Last; i++)
			{
				Row = insrtVal.ElementAt(i);
				Params.Append(Row.Key);
				Values.Append('@');
				Values.Append(Row.Key);
				if (i != Last)
				{
					Params.Append(',');
					Values.Append(',');
				}
			}
			string cmd = "INSERT INTO " + tbl + "(" + Params + ")\nVALUES(" + Values + ")";
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (i = 0; i <= Last; i++)
			{
				Row = insrtVal.ElementAt(i);
				dbCmd.Parameters.AddWithValue("@" + Row.Key, Row.Value);
			}
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
		// add (insert) a row of data to a database (from multi params)
		public int AddRow(string tbl, params object[] insrtVal)
		{
			int i, Last;
			StringBuilder Params = new StringBuilder();
			StringBuilder Values = new StringBuilder();

			Last = insrtVal.Length - 2;
			for (i = 0; i <= Last; i += 2)
			{
				Params.Append(insrtVal[i]);
				Values.Append('@');
				Values.Append(insrtVal[i + 1]);
				if (i != Last)
				{
					Params.Append(',');
					Values.Append(',');
				}
			}
			string cmd = "INSERT INTO " + tbl + "(" + Params + ")\nVALUES(" + Values + ")";
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (i = 0; i <= Last; i += 2)
			{
				dbCmd.Parameters.AddWithValue("@" + insrtVal[i], insrtVal[i + 1]);
			}
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
		// remove (delete) a row from a table with specified search criteria
		public int RemoveRows(string tbl, string delCrt)
		{
			string cmd = "DELETE FROM " + tbl + ((delCrt == null || delCrt.Length == 0) ? "" : "\nWHERE " + delCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
		// remove (delete) a row from a table with specified search criteria and keyvals search parameters
		public int RemoveRows(string tbl, string delCrt, sbKeyVals delParams)
		{
			sbKeyVal Row;

			string cmd = "DELETE FROM " + tbl + ((delCrt == null || delCrt.Length == 0) ? "" : "\nWHERE " + delCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			if (delParams != null)
			{
				for (int i = 0; i < delParams.Count; i++)
				{
					Row = delParams.ElementAt(i);
					dbCmd.Parameters.AddWithValue(Row.Key, Row.Value);
				}
			}
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
		// remove (delete) a row from a table with specified search citeria and multi params search parameters
		public int RemoveRows(string tbl, string delCrt, params object[] delParams)
		{
			string cmd = "DELETE FROM " + tbl + ((delCrt == null || delCrt.Length == 0) ? "" : "\nWHERE " + delCrt);
			SqlCommand dbCmd = new SqlCommand(cmd, dbConn);
			for (int i = 0; i < delParams.Length; i += 2)
			{
				dbCmd.Parameters.AddWithValue((string)delParams[i], delParams[i + 1]);
			}
			RowsAffected = dbCmd.ExecuteNonQuery();
			return (RowsAffected > 0) ? 0 : -1;
		}
	}
}


