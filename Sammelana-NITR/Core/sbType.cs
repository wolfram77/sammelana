﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sammelana_NITR.Core
{

	// Global Enumerations
	public enum sbRes
	{
		Ok,
		PreOk,
		No,
		NoAvail,
		Small,
		Big,
		Early,
		Late
	}
	public enum sbMsgMode
	{
		Post,
		Query,
		ScriptMsg
	}

	public class sbType
	{
	}
}