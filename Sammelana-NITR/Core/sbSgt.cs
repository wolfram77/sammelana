﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbSgt
	{
		// global variables
		// suggestion information expire time (in hours)
		public static int InfTm = int.Parse(ConfigurationManager.AppSettings["Sgt_InfTm"]);

		// Functions
		// clear all read suggestions
		public static void CleanRead(sbDb db)
		{
			db.RemoveRows("Sgt", "Rd = @Rd", "@Rd", true);
		}
		// get all suggestion info from suggestion id
		public static sbKeyVals Fetch(sbDb db, int SgId)
		{
			return db.GetRow("Sgt", "*", "SgId = " + SgId);
		}
		// get all suggestion info from action id and action type
		public static sbKeyVals Fetch(sbDb db, Guid AId, string AcTp)
		{
			int SgId = sbAct.FetchEId(db, AId, AcTp);
			if (SgId < 0) return null;
			return db.GetRow("Sgt", "*", "SgId = " + SgId);
		}
		// add a new suggestion to the system
		public static int AddNew(sbDb db, sbKeyVals Sgt)
		{
			int ret, SgId;
			Guid AId;

			// get next suggestion id
			SgId = sbIds.NextId(db, "Sgt");
			if (SgId < 0) return -1;
			Sgt["SgId"] = SgId;
			// add info to database "Sgt" table
			ret = db.AddRow("Sgt", Sgt);
			if (ret < 0) return -1;
			// update Id to next
			sbIds.Update(db, "Sgt");
			// add new suggestion information action
			AId = sbAct.AddNew(db, SgId, "Suggstn", InfTm);
			if (AId == Guid.Empty) return -1;
			return 0;
		}
	}
}