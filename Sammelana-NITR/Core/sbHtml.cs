﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Collections.Specialized;
using Microsoft.Win32;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	/*
	 * Script <-> Server Communication Protocol on top of JSON
	 * 
	 * {
	 * "M0": msgelement0
	 * "M1": msgelement1
	 * ...
	 * }
	 * 
	 * "msgelement" is an array which can contain the following types
	 * of information:
	 * "K(ey)<Key name>", value, ...
	 * "A(rray)<Array name>", length, val0, val1, ...
	 * "F(unction)<Function name>", num_parameters, "<param0 name>", param0_val, ...
	 * "R(eturn to function)<Function name>", num_parameters, "<param0 name>", param0_val, ...
	 * 
	 */


	public class sbHtml
	{
		// global variables
		// default content type for unrecognized file types
		public static string DfltMime = "application/octet-stream";
		// html script message end key
		public static string MsgEndKey = "ASP.NET_SessionId";
		// content type of raw message
		public static string RawMsgCntntTyp = "text/plain";
		// content type of message
		public static string MsgCntntTyp = "application/json";

		// Functions
		// get the content type for a given filename
		public static string GetContentType(string filename)
		{
			// set the content type as the default content type
			string contentType = DfltMime;
			// get the file extension name in lower case
			string fileExt = Path.GetExtension(filename).ToLowerInvariant();
			// try to get the content type of file of provided extension name, from the registry
			try
			{
				// get the registrykey for the file extension
				RegistryKey fileExtKey = Registry.ClassesRoot.OpenSubKey(fileExt);
				// if registrykey exists for the provided file extension, then get the value
				// of its content type attribute
				if (fileExtKey != null && fileExtKey.GetValue("Content Type") != null)
				{
					contentType = fileExtKey.GetValue("Content Type").ToString();
				}
			}
			// on error, do nothing (return the default content type)
			catch (Exception) { return contentType; }
			// return the determined content type
			return contentType;
		}
		// set a cookie
		public static void SetCookie(HttpRequest hReq, HttpResponse hResp, string cookie, object value, double expHrs)
		{
			HttpCookie _reqCookie = hReq.Cookies[cookie];
			HttpCookie _ck = new HttpCookie(cookie, value.ToString());
			_ck.HttpOnly = false;
			_ck.Expires = DateTime.Now.AddHours(expHrs);
			if (_reqCookie != null) hResp.SetCookie(_ck);
			else hResp.AppendCookie(_ck);
		}
		// send a text response to script
		public static void SendRawMsg(HttpResponse hResp, string msg)
		{
			hResp.Clear();
			if(hResp.ContentType != MsgCntntTyp) hResp.ContentType = RawMsgCntntTyp;
			hResp.Expires = -1;
			hResp.Write(msg);
			hResp.End();
		}
		// recieve a text response from script
		public static string RecvRawMsg(HttpRequest hReq)
		{
			return hReq.BinaryRead(hReq.TotalBytes).ToString();
		}
		// send a file as a response
		public static int SendFile(HttpResponse hResp, string file)
		{
			int ret = 0;
			hResp.Clear();
			hResp.ContentType = GetContentType(file);
			hResp.Expires = 1440;
			try { hResp.TransmitFile(file); }
			catch (Exception) { ret = -1; }
			hResp.End();
			return ret;
		}
		// Send message to script
		public static void SendMsg(HttpResponse hResp, sbKeyVals data)
		{
			SendRawMsg(hResp, sbKv.ToString(data));
		}
		// Recieve message from script
		public static sbKeyVals RecvMsg(HttpRequest hReq)
		{
			string key;
			object trykey;
			string[] kprt, vals;
			sbKeyVals ret, crr;
			int i, j, plen, klen;
			NameValueCollection prms;

			// get request parameters
			prms = hReq.Params;
			plen = prms.Count;
			ret = new sbKeyVals();
			// loop through all parameters
			for (i = 0; i < plen; i++)
			{
				// get the key
				key = prms.GetKey(i);
				if (key == MsgEndKey) break;
				key = key.Replace("]", "");
				kprt = key.Split('[');
				klen = kprt.Length - 1;
				if (kprt[klen] == "") klen--;
				// reach a deeper key level
				crr = ret;
				for (j = 0; j < klen; j++)
				{
					if (crr.TryGetValue(kprt[j], out trykey)) crr = (sbKeyVals)trykey;
					else
					{
						crr[kprt[j]] = new sbKeyVals();
						crr = (sbKeyVals)crr[kprt[j]];
					}
				}
				// get the values
				vals = prms.GetValues(i);
				if (vals == null || vals.Length == 0) crr[kprt[klen]] = null;
				else if (vals.Length == 1) crr[kprt[klen]] = vals[0];
				else crr[kprt[klen]] = vals;
			}
			return ret;
		}
	}
}