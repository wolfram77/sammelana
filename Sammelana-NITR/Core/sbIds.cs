﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sammelana_NITR.Core
{
	public class sbIds
	{
		// get the next id for a particular table (ex usr)
		public static int NextId(sbDb db, string IdTp)
		{
			int? Id = (int?)db.GetValue("Ids", "IdVl", "IdTp = @IdTp", "@IdTp", IdTp);
			if (Id != null) return Id.Value;
			return -1;
		}
		// increment the next id for a table
		public static int Update(sbDb db, string IdTp)
		{
			return db.SetValues("Ids", "IdVl = IdVl + IdStp", "IdTp = @IdTp", "@IdTp", IdTp);
		}
	}
}