﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Sammelana_NITR.Core
{
	public class sbForm
	{
		// global settings
		// get the minimum length of password allowed
		public static int LenMinPwd = int.Parse(ConfigurationManager.AppSettings["Form_Pwd_MinLen"]);
		// get a list of picture file types allowed
		public static string[] TypsPic = ConfigurationManager.AppSettings["Form_Pic_Typs"].Split(new char[] { ',' });
		// get the maximum picture file size allowed
		public static int MaxSzPic = int.Parse(ConfigurationManager.AppSettings["Form_Pic_MaxSz"]) * 1024;
		// get the list of allowed document file types
		public static string[] TypsDoc = ConfigurationManager.AppSettings["Form_Doc_Typs"].Split(new char[] { ',' });
		// get the allowed max size of a document file
		public static int MaxSzDoc = int.Parse(ConfigurationManager.AppSettings["Form_Doc_MaxSz"]) * 1024;
		// string of invalid chracters for email field
		public static string InvEml = "\"\'\n\\~`!#$%^&*()=+[]{}|:;,<>,?/";
		// string of invalid chracters for name field
		public static string InvNm = "`~!@#$%^&*()-_=+[]{}|\\\"\':;<,>?/0123456789";
		// string of invalid chracters for address field
		public static string InvAddr = "\'\"~!$%^?";
		// string of valid chracters for phone field
		public static string VldPhn = "0123456789-+";
		// validator class for display of error
		public static string VldtrClass = "alert";
		// default text blank message
		public static string MsgTxtBlnk = "This field cannot be left blank";
		// default text invalid message
		public static string MsgTxtInv = "Value entered in this field is invalid";

		// validate a text as per its type and type data
		public static sbRes VldtTxt(string txt, string typ, object typd)
		{
			string ext;
			double td;
			DateTime dt;
			int ti, len = txt.Length;
			if (len < 1) return sbRes.Small;
			switch (typ)
			{
				case "Eml":
					if (txt.Length < 6) return sbRes.No;
					if (sbStr.Contains(txt, InvEml)) return sbRes.No;
					string[] Parts = txt.Split(new char[] { '@' });
					if (Parts.Length != 2) return sbRes.No;
					if (Parts[0].Length < 1 || Parts[1].Length < 4) return sbRes.No;
					Parts = Parts[1].Split(new char[] { '.' });
					if (Parts.Length < 2) return sbRes.No;
					for (int i = 0; i < Parts.Length - 1; i++)
						if (Parts[i].Length == 0) return sbRes.No;
					if (Parts[Parts.Length - 1].Length < 2) return sbRes.No;
					break;
				case "Pwd":
					if (txt.Length < LenMinPwd) return sbRes.No;
					break;
				case "Nm":
					if (sbStr.Contains(txt, InvNm)) return sbRes.No;
					break;
				case "Gndr":
					if (txt != "Male" && txt != "Female") return sbRes.No;
					break;
				case "Date":
				case "Time":
				case "DtTm":
					if (!DateTime.TryParse(txt, out dt)) return sbRes.No;
					break;
				case "Addr":
					if (sbStr.Contains(txt, InvAddr)) return sbRes.No;
					break;
				case "Phn":
					if (txt.Length > 16 || !sbStr.ContainsOnly(txt, VldPhn)) return sbRes.No;
					break;
				case "Num":
					if (!double.TryParse(txt, out td)) return sbRes.No;
					break;
				case "Int":
					if (!int.TryParse(txt, out ti)) return sbRes.No;
					break;
				case "Mtch":
					if (txt != (string)typd) return sbRes.No;
					break;
				case "Pic":
					if ((int)typd > MaxSzPic) return sbRes.Big;
					ext = Path.GetExtension(txt).ToLower();
					foreach (string vldExt in TypsPic)
						if (ext == vldExt) return sbRes.Ok;
					return sbRes.No;
				case "Doc":
					if ((int)typd > MaxSzDoc) return sbRes.Big;
					ext = Path.GetExtension(txt).ToLower();
					foreach (string vldExt in TypsDoc)
						if (ext == vldExt) return sbRes.Ok;
					return sbRes.No;
			}
			return sbRes.Ok;
		}
		// validate a text field with provided error messages
		public static bool VldtFld(string txt, string typ, object typd, Label vldtr, string txt_blnk, string txt_inv)
		{
			bool vld = false;
			sbRes vn = VldtTxt(txt, typ, typd);
			switch (vn)
			{
				case sbRes.Small:
					if (txt_blnk != "" && vldtr != null) { vldtr.Text = txt_blnk; vldtr.CssClass = VldtrClass; }
					break;
				case sbRes.No:
					if (txt_inv != "" && vldtr != null) { vldtr.Text = txt_inv; vldtr.CssClass = VldtrClass; }
					break;
			}
			if (vn == sbRes.Ok || (vn == sbRes.Small && txt_blnk == "") || (vn == sbRes.No && txt_inv == "")) vld = true;
			return vld;
		}
		// validate a text field with automatic / default error messages
		public static bool VldtFldAuto(string txt, string typ, object typd, Label vldtr)
		{
			return VldtFld(txt, typ, typd, vldtr, MsgTxtBlnk, MsgTxtInv);
		}
		// validate a text field with automatic / default error message for blank text only
		public static bool VldtFldAutoBlnk(string txt, string typ, object typd, Label vldtr)
		{
			return VldtFld(txt, typ, typd, vldtr, MsgTxtBlnk, "");
		}
		// validate a text field with automatic / default error message for invalid text only
		public static bool VldtFldAutoInv(string txt, string typ, object typd, Label vldtr)
		{
			return VldtFld(txt, typ, typd, vldtr, "", MsgTxtInv);
		}
		// report a validation problem
		public static void VldtrReport(Label vldtr, string txt)
		{
			vldtr.Text = txt;
			vldtr.CssClass = VldtrClass;
		}
		// get gender from selected index
		public static string GetGender(int sel_indx)
		{
			if (sel_indx < 0) return "";
			return (sel_indx == 0) ? "M" : "F";
		}
		// get date time from date string
		public static DateTime GetDateTime(string dttm_str)
		{
			DateTime tmp = DateTime.MinValue;
			DateTime.TryParse(dttm_str, out tmp);
			return tmp;
		}
		// get the guid from guid string
		public static Guid GetGuid(string guid)
		{
			Guid ret = Guid.Empty;
			Guid.TryParse(guid, out ret);
			return ret;
		}
	}
}
