﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Sammelana_NITR.Core
{
	public class sbServer
	{
		// global constants
		// global web address of the server
		public static string Addr = ConfigurationManager.AppSettings["Srvr_Addr"];
		// no. of max. users allowed to access server at the same time
		public static string MaxUsr = ConfigurationManager.AppSettings["Srvr_MaxUsr"];
	}
}