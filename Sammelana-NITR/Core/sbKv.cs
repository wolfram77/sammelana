﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbKv
	{
		// adds key value pairs
		public static sbKeyVals Add(sbKeyVals src_kv, params object[] pairs)
		{
			int i, len;

			len = pairs.Length;
			for (i = 0; i < len; i += 2)
			{
				src_kv[(string)pairs[i]] = pairs[i + 1];
			}
			return src_kv;
		}
		// removes key value pairs
		public static sbKeyVals Remove(sbKeyVals src_kv, string[] keys)
		{
			int i, len;

			len = keys.Length;
			for (i = 0; i < len; i++)
			{
				src_kv.Remove(keys[i]);
			}
			return src_kv;
		}
		// try get string value of key
		public static string GetStrVal(sbKeyVals src_kv, string key)
		{
			object ret = "";

			if (src_kv == null) return "";
			src_kv.TryGetValue(key, out ret);
			return ret.ToString();
		}
		// convert an sbkeyvals object to string
		public static string ToString(sbKeyVals src_kv)
		{
			Type typ;
			bool isarr;
			object val;
			Array arrs;
			sbKeyVals ptrs;
			List<object> stck;
			int i, dlen, slen;
			StringBuilder ret;

			isarr = false;
			arrs = null;
			ptrs = src_kv;
			i = -1;
			dlen = ptrs.Count;
			ret = new StringBuilder();
			stck = new List<object>();
			while (true)
			{
				i++;
				// on end to limit, pop previous state from stack
				if (i >= dlen)
				{
					if (isarr) ret.Append(']');
					else ret.Append('}');
					slen = stck.Count;
					if (slen == 0) break;
					isarr = (bool)stck[slen - 1];
					dlen = (int)stck[slen - 2];
					i = (int)stck[slen - 3];
					if (isarr) arrs = (Array)stck[slen - 4];
					else ptrs = (sbKeyVals)stck[slen - 4];
					stck.RemoveRange(slen - 4, 4);
					continue;
				}
				if (i > 0) ret.Append(',');
				if (isarr) val = arrs.GetValue(i);
				else
				{
					val = ptrs.ElementAt(i);
					ret.AppendFormat("\"{0}\":", ((sbKeyVal)val).Key);
					val = ((sbKeyVal)val).Value;
				}
				// on new branch push current state and jump to that branch
				typ = val.GetType();
				if (typ.BaseType == typeof(Array))
				{
					if (isarr) stck.Add(arrs);
					else stck.Add(ptrs);
					stck.Add(i);
					stck.Add(dlen);
					stck.Add(isarr);
					ret.Append('[');
					arrs = (Array)val;
					i = -1;
					dlen = arrs.GetLength(0);
					isarr = true;
					continue;
				}
				else if (typ == typeof(sbKeyVals))
				{
					if (isarr) stck.Add(arrs);
					else stck.Add(ptrs);
					stck.Add(i);
					stck.Add(dlen);
					stck.Add(isarr);
					ret.Append('{');
					ptrs = (sbKeyVals)val;
					i = -1;
					dlen = ptrs.Count;
					isarr = false;
					continue;
				}
				// add value
				if (val.GetType() == typeof(string)) ret.AppendFormat("\"{0}\"", val);
				else ret.AppendFormat("{0}", val);
			}
			// insert start bracket
			ret.Insert(0, '{');
			return ret.ToString();
		}
		// convert name value collection to sbkeyvals
		public static sbKeyVals ToKeyVals(NameValueCollection src_nv)
		{
			int i, len;
			sbKeyVals ret;

			len = src_nv.Count;
			ret = new sbKeyVals();
			for (i = 0; i < len; i++)
			{
				ret[src_nv.GetKey(i)] = src_nv.Get(i);
			}
			return ret;
		}
		// to keyvals from array keyvals
		public static sbKeyVals ToKeyVals(sbKeyVals[] src_kv_arr)
		{
			int i, len;
			sbKeyVals ret;

			ret = new sbKeyVals();
			len = src_kv_arr.Length;
			for (i = 0; i < len; i++)
			{
				ret[(string)src_kv_arr[i]["Key"]] = src_kv_arr[i]["Val"];
			}
			return ret;
		}
		// convert an sqldatareader object to keyvals object
		public static sbKeyVals ToKeyVals(SqlDataReader row)
		{
			List<sbKeyVals> kv = ToLstKeyVals(row, false);
			return (kv == null)? null: kv[0];
		}
		// to array keyvals (for orderdered keyvals)
		public static sbKeyVals[] ToArrKeyVals(sbKeyVals src_kv)
		{
			int i, len;
			sbKeyVals[] ret;
			sbKeyVal kv;

			len = src_kv.Count;
			ret = new sbKeyVals[len];
			for (i = 0; i < len; i++)
			{
				ret[i] = new sbKeyVals();
				kv = src_kv.ElementAt(i);
				ret[i]["Key"] = kv.Key;
				ret[i]["Val"] = kv.Value;
			}
			return ret;
		}
		// convert an sqldatareader object to keyvals array
		public static List<sbKeyVals> ToLstKeyVals(SqlDataReader rows, bool multiple_rows)
		{
			int i;
			sbKeyVals kvRow;
			List<sbKeyVals> kvRows = new List<sbKeyVals>();

			if (rows == null) return null;
			if (!rows.Read())
			{
				rows.Close();
				return null;
			}
			do
			{
				kvRow = new sbKeyVals();
				for (i = 0; i < rows.FieldCount; i++)
				{
					kvRow[rows.GetName(i)] = rows.GetValue(i);
				}
				kvRows.Add(kvRow);
			} while (rows.Read() && multiple_rows);
			rows.Close();
			return kvRows;
		}
	}
}
