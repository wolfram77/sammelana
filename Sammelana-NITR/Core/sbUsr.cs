﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbUsr
	{
		// global variables
		// hours after which user sign up confirmation code will expire
		public static int SgnUpCnfTm = int.Parse(ConfigurationManager.AppSettings["Usr_SgnUpCnfTm"]);
		// hours after which user sign in will expire
		public static int SgnInTm = int.Parse(ConfigurationManager.AppSettings["Usr_SgnInTm"]);
		// filename prefix for picture
		public static string PicPrfx = ConfigurationManager.AppSettings["Usr_PicPrfx"];
		// filename prefix for CV
		public static string CvPrfx = ConfigurationManager.AppSettings["Usr_CvPrfx"];
		// sign up confirmation mail subject and body
		public static string SgnUpCnfMail = ConfigurationManager.AppSettings["Usr_SgnUpCnfMail"];
		// sign up confirmation mail subject only
		public static string SgnUpCnfMailSub = SgnUpCnfMail.Substring(0, SgnUpCnfMail.IndexOf('|'));
		// sign up confirmation mail body only
		public static string SgnUpCnfMailBdy = SgnUpCnfMail.Substring(SgnUpCnfMail.IndexOf('|') + 1);
		// the action id that was last accessed (for login purpose)
		public static string LastUsrId;
		// all user info of last signed in user
		public static sbKeyVals LastUsr;

		// get user name from user details
		public static string GetName(sbKeyVals Usr)
		{
			string nm = "";

			if (Usr != null)
			{
				nm = (string)Usr["FtNm"];
				nm += (((string)Usr["FtNm"]).Length > 0) ? " " + Usr["MdNm"] : Usr["MdNm"];
				nm += (((string)Usr["MdNm"]).Length > 0) ? " " + Usr["LtNm"] : Usr["LtNm"];
			}
			return nm;
		}
		// get the path of a user picture
		public static string GetPic(sbKeyVals Usr, bool web_path)
		{
			if (Usr == null || (string)Usr["Pic"] == "") return "";
			if (web_path) return sbStore.GetFullPath("Usr", (string)Usr["Pic"], true);
			return "/Pages/Access.aspx?attr=usr&dat=" + Usr["Pic"];
		}
		// get all user info from user id
		public static sbKeyVals Fetch(sbDb db, int UId)
		{
			return db.GetRow("Usr", "*", "UId = " + UId);
		}
		// get all user info from action id and action type
		public static sbKeyVals Fetch(sbDb db, Guid AId, string AcTp)
		{
			int UId = sbAct.FetchEId(db, AId, AcTp);
			if (UId < 0) return null;
			return db.GetRow("Usr", "*", "UId = " + UId);
		}
		// get all user info from email-id of the user
		public static sbKeyVals Fetch(sbDb db, string Eml)
		{
			return db.GetRow("Usr", "*", "Eml = @Eml", "@Eml", Eml);
		}
		// get the details of signed in user
		public static sbKeyVals GetSignedIn(sbDb db, string login_id)
		{
			Guid AId;

			if (login_id == "")
			{
				LastUsr = null;
				LastUsrId = "";
			}
			else if (login_id != LastUsrId)
			{
				// set the current login_id
				LastUsrId = login_id;
				// get login GUID
				try
				{
					AId = new Guid(login_id);
					LastUsr = Fetch(db, AId, "UsrSgnIn");
				}
				catch (Exception)
				{
					LastUsr = null;
					LastUsrId = "";
				}
			}
			return LastUsr;
		}
		// match user credentials (unavailable, unacceptable, acceptable)
		public static sbRes MatchCred(sbDb db, string Eml, string Pwd)
		{
			string dPwd = (string)db.GetValue("Usr", "Pwd", "Eml = @Eml", "@Eml", Eml);
			if (dPwd == null) return sbRes.NoAvail;
			else if (dPwd != Pwd) return sbRes.No;
			return sbRes.Ok;
		}
		// add a new user to the system
		public static int AddNew(sbDb db, sbKeyVals Usr, byte[] UsrPicData, byte[] UsrCvData)
		{
			int ret, UId;
			Guid AId;

			// get next user id
			UId = sbIds.NextId(db, "Usr");
			if (UId < 0) return -1;
			Usr["UId"] = UId;
			// save user's picture and CV to store
			if ((string)Usr["Pic"] != "" && UsrPicData != null && UsrPicData.Length > 0) Usr["Pic"] = sbStore.SaveFile(sbStore.Usr_PhyPath, PicPrfx, UId, (string)Usr["Pic"], UsrPicData);
			if ((string)Usr["CV"] != "" && UsrCvData != null && UsrCvData.Length > 0) Usr["CV"] = sbStore.SaveFile(sbStore.Usr_PhyPath, CvPrfx, UId, (string)Usr["CV"], UsrCvData);
			// add info to database "Usr" table
			ret = db.AddRow("Usr", Usr);
			if (ret < 0) return -1;
			// update Id to next
			sbIds.Update(db, "Usr");
			// add user sign up confirmation action
			AId = sbAct.AddNew(db, UId, "UsrSgnUp", SgnUpCnfTm);
			if (AId == Guid.Empty) return -1;
			// send confirmation email
			Usr["SgnUpCnfLink"] = sbMail.FetchCnfLink(AId);
			ret = sbMail.Send((string)Usr["Eml"], sbStr.Replace(SgnUpCnfMailSub, Usr, false), sbStr.Replace(SgnUpCnfMailBdy, Usr, false));
			Usr.Remove("SgnUpCnfLink");
			if (ret < 0)
			{
				sbAct.Remove(db, AId);
				return -1;
			}
			return 0;
		}
		// confirm the sign up process of a user
		public static int AddCnf(sbDb db, Guid AId)
		{
			sbKeyVals dAct = sbAct.Fetch(db, AId);
			if (dAct == null) return -1;
			sbAct.Remove(db, AId);
			db.SetValues("Usr", "AcVf = 1", "UId = @UId", "@UId", dAct["EId"]);
			return 0;
		}
	}
}
