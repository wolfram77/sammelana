﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbCnf
	{
		// global variables
		// filename prefix for conference information
		public static string InfPrfx = ConfigurationManager.AppSettings["Cnf_InfPrfx"];
		// conference admin mail id for confirming new conferences
		public static string RegCnfAddr = ConfigurationManager.AppSettings["Cnf_RegCnfAddr"];
		// registration confirmation mail subject and body
		public static string RegCnfMail = ConfigurationManager.AppSettings["Cnf_RegCnfMail"];
		// registration confirmation mail subject only
		public static string RegCnfMailSub = RegCnfMail.Substring(0, RegCnfMail.IndexOf('|'));
		// registration confirmation mail body only
		public static string RegCnfMailBdy = RegCnfMail.Substring(RegCnfMail.IndexOf('|') + 1);

		// get the conference id for given conference acronym
		public static int FetchCId(sbDb db, string Acr)
		{
			object CId = db.GetValue("Cnf", "CId", "Acr = @Acr", "@Acr", Acr);
			if (CId == null) return -1;
			return (int)CId;
		}
		// get the user id for given conference acronym
		public static int FetchUId(sbDb db, string Acr)
		{
			object UId = db.GetValue("Cnf", "UId", "Acr = @Acr", "@Acr", Acr);
			if (UId == null) return -1;
			return (int)UId;
		}
		// get all conference info from given conference id
		public static sbKeyVals Fetch(sbDb db, int CId)
		{
			return db.GetRow("Cnf", "*", "CId = " + CId);
		}
		// get all conference info from action id and action type
		public static sbKeyVals Fetch(sbDb db, Guid AId, string AcTp)
		{
			int CId = sbAct.FetchEId(db, AId, AcTp);
			if (CId < 0) return null;
			return db.GetRow("Cnf", "*", "CId = " + CId);
		}
		// get all conference info from given conference acronym
		public static sbKeyVals Fetch(sbDb db, string Acr)
		{
			return db.GetRow("Cnf", "*", "Acr = @Acr", "@Acr", Acr);
		}
		// add a new conference to the system
		public static int AddNew(sbDb db, sbKeyVals Cnf, byte[] CnfInfData)
		{
			int ret, CId;
			Guid AId;

			// get next conference id
			CId = sbIds.NextId(db, "Cnf");
			if (CId < 0) return -1;
			Cnf["CId"] = CId;
			// save conference information to store
			if ((string)Cnf["Inf"] != "" && CnfInfData != null && CnfInfData.Length > 0) Cnf["Inf"] = sbStore.SaveFile(sbStore.Cnf_PhyPath, InfPrfx, CId, (string)Cnf["Inf"], CnfInfData);
			// add info to database "Cnf" table
			ret = db.AddRow("Cnf", Cnf);
			if (ret < 0) return -1;
			// update Id to next
			sbIds.Update(db, "Cnf");
			// add conference registration confirmation action
			AId = sbAct.AddNew(db, CId, "CnfReg", sbUsr.SgnUpCnfTm);
			if (AId == Guid.Empty) return -1;
			// send confirmation email to conference admin (as of now)
			Cnf["RegCnfLink"] = sbMail.FetchCnfLink(AId);
			ret = sbMail.Send(RegCnfAddr, sbStr.Replace(RegCnfMailSub, Cnf, true), sbStr.Replace(RegCnfMailBdy, Cnf, true));
			Cnf.Remove("RegCnfLink");
			if (ret < 0)
			{
				sbAct.Remove(db, AId);
				return -1;
			}
			return 0;
		}
		// confirm the registration of a conference
		public static int AddCnf(sbDb db, Guid AId)
		{
			sbKeyVals dAct = sbAct.Fetch(db, AId);
			if (dAct == null) return -1;
			sbAct.Remove(db, AId);
			db.SetValues("Cnf", "CfVf = 1", "CId = @CId", "@CId", dAct["EId"]);
			return 0;
		}
	}
}