﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sammelana_NITR.Core
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public class sbStr
	{
		// global variables
		// character which are not part of a variable
		public static string NonVar = " `~!@#$%^&*()-+=[{]};:\'\",<.>/?";
		public static sbKeyVals EscSeq = new sbKeyVals() { { "\\\\", "\\" }, { "\\\'", "\'" }, { "\\\"", "\"" }, { "\\n", "\n" }, { "\\r", "\r" }, { "\\t", "\t" } };
		public static sbKeyVals ClosingChars = new sbKeyVals() { { "(", ")" }, { "[", "]" }, { "{", "}" } };
		public static string Encaps = "([{\'\"";
		public static string ParamSep = ",";

		// Checks if a string contains any of the given characters for comparision
		public static bool Contains(string src_str, string cmp_chars)
		{
			// loop throung all comparision characters
			for (int i = 0; i < cmp_chars.Length; i++)
			{
				// check if the string contains the comparision character, if yes then instantly say found
				if (src_str.Contains(cmp_chars[i])) return true;
			}
			// not found any of the comparision characters in the string
			return false;
		}
		// Checks if a string contains only characters from the given comparision characters
		public static bool ContainsOnly(string src_str, string cmp_chars)
		{
			// loop through the entire string
			for (int i = 0; i < src_str.Length; i++)
			{
				// check if the current character in the is not one of the comparision character, if not then instantly report no
				if (!cmp_chars.Contains(src_str[i])) return false;
			}
			// all characters in the string use only the characters provided in the comparision string
			return true;
		}
		// Replaces all string parameters with its values
		public static string Replace(string src_str, sbKeyVals rep_params, bool no_braces)
		{
			int i, len;
			sbKeyVal prm;
			string ret = src_str;

			len = rep_params.Count;
			for (i = 0; i < len; i++)
			{
				prm = rep_params.ElementAt(i);
				if (no_braces) ret = ret.Replace(prm.Key, prm.Value.ToString());
				else ret = ret.Replace("{" + prm.Key + "}", prm.Value.ToString());
			}
			return ret;
		}
		// convert a string to a possible datatype (includes a variable "Var")
		public static object ToObject(string src_str)
		{
			bool tb;
			int ti;
			double td;
			string s;

			s = src_str.Trim();
			if (bool.TryParse(s, out tb)) return tb;
			if (int.TryParse(s, out ti)) return ti;
			if (double.TryParse(s, out td)) return td;
			if ((s.StartsWith("\"") && s.EndsWith("\"")) || (s.StartsWith("\'") && s.EndsWith("\'"))) return s.Substring(1, s.Length - 2);
			if (!sbStr.Contains(s, NonVar)) return new sbKeyVals() { { "Var", s } };
			return null;
		}
		// finds the index of the closing character for the starting character at string index
		public static int FindClosing(string src_str, int start, bool ignr_escseq, bool in_str)
		{
			char cl;
			bool isstr;
			int i, j, len;
			object ocl, t;

			isstr = false;
			cl = src_str[start];
			len = src_str.Length;
			ocl = src_str[start];
			ClosingChars.TryGetValue(ocl.ToString(), out ocl);
			if (ocl != null) cl = (char)ocl;
			for (i = start + 1; i < len; i++)
			{
				if (!ignr_escseq && src_str[i] == '\\') { i++; continue; }
				if (!in_str && (src_str[i] == '\'' || src_str[i] == '\"')) { isstr = !isstr; continue; }
				if (isstr) continue;
				if (ClosingChars.TryGetValue(src_str[i].ToString(), out t))
				{
					j = FindClosing(src_str, i, ignr_escseq, in_str);
					if (j >= 0) { i = j; continue; }
				}
				if (src_str[i] == cl) return i;
			}
			return -1;
		}
		// splits a given string to a string array using a list of separators
		public static List<string> Split(string src_str, string sep)
		{
			int i, j, k, len;
			List<string> blk;

			len = src_str.Length;
			blk = new List<string>();
			for (i = 0, j = 0; j < len; j++)
			{
				if (Contains(src_str[j].ToString(), Encaps))
				{
					k = FindClosing(src_str, j, false, false);
					if (k >= 0) { j = k; continue; }
				}
				if (j == len - 1 || Contains(src_str[j].ToString(), sep))
				{
					blk.Add(src_str.Substring(i, j - i).Trim());
					i = j + 1;
					continue;
				}
			}
			return blk;
		}
	}
}
