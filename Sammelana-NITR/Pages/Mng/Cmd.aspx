﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Flat.Master" AutoEventWireup="true" CodeBehind="Cmd.aspx.cs" Inherits="Sammelana_NITR.Pages.Mng.Cmd" %>
<asp:Content ID="Cmd_Head" ContentPlaceHolderID="Flat_Head" runat="server">
<script type="text/javascript">
	// on load
	$(document).ready(function () {
		// when enter is pressed, submit form
		$("#<%= Cmnd.ClientID %>").keypress(function (event) {
			if (event.which == 13) $("#<%= Sbmt.ClientID %>").click();
		});
	});
</script>
<style type="text/css">
    body { background-color: Black; }
</style>
</asp:Content>
<asp:Content ID="Cmd_Body" ContentPlaceHolderID="Flat_Body" runat="server">
	<form id="Cmd" runat="server">
	<div class="sbCmd">
		<div class="sbCmdHdr">
			Super Console - Sammelana
		</div>
		<div class="sbCmdTxt">
			<asp:Label ID="Prmpt" runat="server" Text="sammelana&gt;"></asp:Label>
			<br />
			<asp:TextBox ID="Cmnd" runat="server" TextMode="MultiLine" Rows="2" CssClass="sbCmdTxt input-block-level" BorderColor="Black" ></asp:TextBox>
		</div>
		<div class="sbCmdOut" id="Out" runat="server">
            <table></table>
		</div>
		<asp:Button ID="Sbmt" runat="server" Text="Submit" CssClass="hidden" 
			onclick="Sbmt_Click" />
	</div>
	</form>
</asp:Content>
