﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Mng
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Cmd : System.Web.UI.Page
	{


		// Functions
		// generate output as a table
		protected string GenOut(List<sbKeyVals> outp)
		{
			int i, j;
			StringBuilder res;
			sbKeyVal rw;

			res = new StringBuilder();
			if (outp == null || outp.Count == 0)
			{
				return "Invalid Command";
			}
			// insert table header
			res.Append("<tr>");
			for (i = 0; i < outp[0].Count; i++)
			{
				rw = outp[0].ElementAt(i);
				res.AppendFormat("<th>{0}</th>", rw.Key);
			}
			res.Append("</tr>");
			// insert table data
			for (i = 0; i < outp.Count; i++)
			{
				res.Append("<tr>");
				for (j = 0; j < outp[i].Count; j++)
				{
					rw = outp[i].ElementAt(j);
					res.AppendFormat("<td>{0}</td>", rw.Value);
				}
				res.Append("</tr>");
			}
			// insert table
			res.Insert(0, "<table>");
			res.Append("</table>");
			return res.ToString();
		}
		// display output
		protected void DispOut(List<sbKeyVals> outp)
		{
			Out.InnerHtml = "sammelana&gt;<br />" + Cmnd.Text + "<br /><br />" + GenOut(outp) + "<hr />" + Out.InnerHtml;
			Cmnd.Text = "";
		}

		// Event handlers
		protected void Page_Load (object sender, EventArgs e)
		{
		}
		protected void Sbmt_Click(object sender, EventArgs e)
		{
			int prms;
			string[] cmd;
			List<sbKeyVals> Outp = null;

			cmd = Cmnd.Text.Split('#');
			prms = cmd.Length - 1;
			switch (cmd[0].ToLower())
			{
				case "db.cstmcmd":
					if (prms < 1) break;
					sbDb db = new sbDb();
					Outp = db.CstmCmd(cmd[1]);
					db.Disconnect();
					break;
			}
			DispOut(Outp);
		}
	}
}
