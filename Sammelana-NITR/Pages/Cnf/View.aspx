﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="View.aspx.cs" Inherits="Sammelana_NITR.Pages.Cnf.View" %>
<asp:Content ID="View_Head" ContentPlaceHolderID="Main_Head" runat="server">
	<title>Conferences - Sammelana</title>
	<link href="/plugin/jquery-bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
	<script src="/plugin/jquery-bxslider/jquery.bxslider.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="View_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<script type="text/javascript">
	    $(document).ready(function () {
	        $(".bxslider").bxSlider({
	            adaptiveHeight: true,
	            auto: true,
	            captions: false
	        });
	    });
	</script>
    <div class="PgGap mid">
        <div style="padding-bottom: 40px; margin: 0 auto; width: 533px;">
	    <ul class="bxslider">
		    <li><img src="/Data/misc/img1.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img2.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img3.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img4.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img5.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img6.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img7.jpg" alt="" /></li>
		    <li><img src="/Data/misc/img8.jpg" alt="" /></li>
	    </ul>
        </div>



    <h4>This is where description about the detailed Conferences being held at Sammelena will be displayed.</h4>
    <p>Be innovative, plus a lot of code is going to be needed here, the page should update dynamically (like FB).
       We can uses jQuery AJAX for that purpose. Big time coders can do this work quite easily.</p>
    </div>
</asp:Content>
