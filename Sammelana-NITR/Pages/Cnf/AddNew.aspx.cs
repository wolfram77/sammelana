﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Cnf
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class AddNew : System.Web.UI.Page
	{
		// Form Data is stored here
		sbKeyVals Cnf;
		byte[] CnfInfData;
		// Request Data
		string rqReq, rqAcr;

		// functions
		// replies to an acronym check request from script
		protected void Msg_RegChk (sbDb db, string Acr)
		{
			if (sbCnf.FetchCId(db, Acr) >= 0) sbHtml.SendRawMsg(Response, "");
			else sbHtml.SendRawMsg(Response, "Acronym unavailable|This acronym has already been used for another conference");
		}
		// gets all the info entered by user into Cnf keyvals (registration)
		protected void GetCnfInfo()
		{
			Cnf = new sbKeyVals();
			// fill in with all the details
			Cnf["CId"] = 0;
			Cnf["Eml"] = Acr.Text;
			Cnf["CfVf"] = false;
			Cnf["Pwd"] = Pwd.Text;
			Cnf["UId"] = 0;
			Cnf["Ttl"] = Ttl.Text;
			Cnf["Cat"] = Cat.Text;
			Cnf["BgTm"] = sbForm.GetDateTime(BgDt.Text + " " + BgTm.Text);
			Cnf["EdTm"] = sbForm.GetDateTime(EdDt.Text + " " + EdTm.Text);
			Cnf["Dsc"] = Dsc.Text;
			Cnf["Inf"] = Inf.FileName;
			CnfInfData = Inf.FileBytes;
			Cnf["AdAt"] = AdAt.Text;
			Cnf["AdAr"] = AdAr.Text;
			Cnf["AdCt"] = AdCt.Text;
			Cnf["AdSt"] = AdSt.Text;
			Cnf["AdCn"] = AdCn.Text;
			Cnf["AdPn"] = AdPn.Text;
		}
		// validate all conference information saved into Cnf keyvals and remove the unrequired fields
		protected bool VldtUsrInfo(sbDb db)
		{
			// assume the conference info to be valid first
			bool valid = true;
			int CId;

			// perform thorough acronym validity check
			if (!sbForm.VldtFldAuto(Acr.Text, "address", "", CCAcr)) valid = false;
			else
			{
				CId = sbCnf.FetchCId(db, Acr.Text);
				if (CId > 0)
				{
					valid = false;
					sbForm.VldtrReport(CCAcr, "Another conference with this acronym is already registered");
				}
			}
			// validate all other fields normally
			valid = (sbForm.VldtFld(Pwd.Text, "pwd", "", CCPwd, "", "Password must be 6 characters or more")) ? valid : false;
			if(Pwd.Text != "") valid = (sbForm.VldtFld(CPwd.Text, "mtch", Pwd.Text, CCCPwd, "Confirm password here", "Passwords dont match")) ? valid : false;
			valid = (sbForm.VldtFldAuto(Ttl.Text, "addr", "", CCTtl)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Cat.Text, "nm", "", CCCat)) ? valid : false;
			valid = (sbForm.VldtFldAuto(BgDt.Text, "date", "", CCBgDt)) ? valid : false;
			valid = (sbForm.VldtFldAuto(BgTm.Text, "time", "", CCBgTm)) ? valid : false;
			valid = (sbForm.VldtFldAuto(EdDt.Text, "date", "", CCEdDt)) ? valid : false;
			valid = (sbForm.VldtFldAuto(EdTm.Text, "time", "", CCEdTm)) ? valid : false;
			valid = (sbForm.VldtFldAuto(Dsc.Text, "addr", "", CCDsc)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Inf.FileName, "doc", CnfInfData.Length, CCInf)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdAt.Text, "addr", "", CCAdAt)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdAr.Text, "addr", "", CCAdAr)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdCt.Text, "addr", "", CCAdCt)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdSt.Text, "addr", "", CCAdSt)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdCn.Text, "addr", "", CCAdCn)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdPn.Text, "addr", "", CCAdPn)) ? valid : false;
			// return whether all of the entered info is valid or not
			return valid;
		}

		// event handlers
		protected void Page_Load(object sender, EventArgs e)
		{
			rqReq = Request["Req"];
			rqReq = ( rqReq == null ) ? "" : rqReq;
			if (rqReq == "RegChk")
			{
				rqAcr = Request["Acr"];
				rqAcr = ( rqAcr == null ) ? "" : rqAcr;
				sbDb db = new sbDb();
				Msg_RegChk(db, rqAcr);
				db.Disconnect();
			}
		}
		protected void Sbmt_Click(object sender, EventArgs e)
		{
			int ret;

			sbDb db = new sbDb();
			GetCnfInfo();
			if (VldtUsrInfo(db))
			{
				ret = sbCnf.AddNew(db, Cnf, CnfInfData);
				if (ret == 0) Response.Redirect("/Pages/Users/ConfVerify.aspx?md=vrsnt");
				else Response.Redirect("/Pages/Users/ConfVerify.aspx");
			}
			db.Disconnect();
		}
	}
}