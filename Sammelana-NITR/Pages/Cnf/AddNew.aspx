﻿<%@ Page Title="Create Conference - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="Sammelana_NITR.Pages.Cnf.AddNew" %>
<asp:Content ID="AddNew_Head" ContentPlaceHolderID="Main_Head" runat="server">
	<script type="text/javascript">
	<!--
		// Updates the input form box to the requested step
		function sbUpdateStep(stp) {
			for (var i = 1; i <= 3; i++) {
				if (i == stp) continue;
				$("#CCStep" + i).hide();
			}
			if (stp > 1) $("#<%= Bck.ClientID %>").css("visibility", "visible");
			else $("#<%= Bck.ClientID %>").css("visibility", "hidden");
			if (stp == 3) {
				$("#<%= Fwd.ClientID %>").removeClass("btn-primary");
				$("#<%= Fwd.ClientID %>").addClass("btn-success");
				$("#<%= Fwd.ClientID %>").val("Sign up");
			}
			else {
				$("#<%= Fwd.ClientID %>").removeClass("btn-success");
				$("#<%= Fwd.ClientID %>").addClass("btn-primary");
				$("#<%= Fwd.ClientID %>").val("Continue");
			}
			$("#CCStep" + stp).show("fade");
		}
		// Execute on Load
		$(document).ready(function () {
			// Design the document
			$("#<%= BgDt.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, minDate: "+0Y", maxDate: "+4Y", yearRange: "c:c+4" });
			$("#<%= EdDt.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, minDate: "+0Y", maxDate: "+4Y", yearRange: "c:c+4" });
			$("#<%= BgTm.ClientID %>").timepickr({ convention: 12 });
			$("#<%= EdTm.ClientID %>").timepickr({ convention: 12 });
			sbSetPopupDir("left");

			// Create Conference Steps
			var step = 1;
			var btn_nxt = false;
			var acr_allwd = false;
			var sbmt = true;

			// Goto the first step
			sbUpdateStep(step);

			// Ensure Client side Form Validation
			$("#<%= Bck.ClientID %>").click(function () { btn_nxt = false; });
			$("#<%= Fwd.ClientID %>").click(function () { btn_nxt = true; });
			$("#<%= CC.ClientID %>").submit(function () {
				switch (step) {
					case 1:
						var ttl = sbVldtFld("#<%= Ttl.ClientID %>", "address", "", "#CCTtl1", "#CCTtl2");
						var acr = sbVldtFld("#<%= Acr.ClientID %>", "address", "", "#CCAcr1", "#CCAcr2");
						var dsc = sbVldtFld("#<%= Dsc.ClientID %>", "address", "", "#CCDsc1", "#CCDsc2");
						if (!btn_nxt) sbUpdateStep(--step);
						else {
							var v_acr = $("#<%= Acr.ClientID %>").val();
							if (ttl && acr && dsc) {
								$.post("/Pages/Users/CreateConf.aspx", { "Req": "RegChk", "Acr": v_acr }, function (resp) {
									if (resp != "") {
										sbShowPopup("#<%= Acr.ClientID %>", resp);
										acr_allwd = false;
									}
									else {
										acr_allwd = true;
										sbUpdateStep(++step);
									}
								});
							}
						}
						break;
					case 2:
						var v_pwd = $("#<%= Pwd.ClientID %>").val();
						var pwd = sbVldtFld("#<%= Pwd.ClientID %>", "password", "", "", "#CCPwd2");
						var cpwd = true;
						if (v_pwd != "") cpwd = sbVldtFld("#<%= CPwd.ClientID %>", "match", $("#<%= Pwd.ClientID %>").val(), "#CCCPwd1", "#CCCPwd2");
						var cat = sbVldtFld("#<%= Cat.ClientID %>", "name", "", "#CCCat1", "#CCCat2");
						var bgdt = sbVldtFld("#<%= BgDt.ClientID %>", "date", "", "#CCBgDt1", "#CCBgDt2");
						var bgtm = sbVldtFld("#<%= BgTm.ClientID %>", "time", "", "#CCBgTm1", "#CCBgTm2");
						var eddt = sbVldtFld("#<%= EdDt.ClientID %>", "date", "", "#CCEdDt1", "#CCEdDt2");
						var edtm = sbVldtFld("#<%= EdTm.ClientID %>", "time", "", "#CCEdTm1", "#CCEdTm2");
						if (!btn_nxt) sbUpdateStep(--step);
						else { if (pwd && cpwd && cat && bgdt && bgtm && eddt && edtm) sbUpdateStep(++step); }
						break;
					case 3:
						var adat = sbVldtFld("#<%= AdAt.ClientID %>", "address", "", "", "#CCAdAt2");
						var adar = sbVldtFld("#<%= AdAr.ClientID %>", "address", "", "", "#CCAdAr2");
						var adct = sbVldtFld("#<%= AdCt.ClientID %>", "address", "", "", "#CCAdCt2");
						var adst = sbVldtFld("#<%= AdSt.ClientID %>", "address", "", "", "#CCAdSt2");
						var adcn = sbVldtFld("#<%= AdCn.ClientID %>", "address", "", "", "#CCAdCn2");
						var adpn = sbVldtFld("#<%= AdPn.ClientID %>", "integer", "", "", "#CCAdPn2");
						if (!btn_nxt) { sbUpdateStep(--step); }
						else if (adat && adar && adct && adst && adcn && adpn) { if (sbmt) { sbmt = false; return true; } }
						break;
				}
				return false;
			});
		});
	-->
	</script>
</asp:Content>
<asp:Content ID="AddNew_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<div class="thumbnails">
		<div class="span7">
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="span2"><img src="/Data/img/signup/call4papers.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					It is required to sign up here before submitting paper to a conference that is supported by Sammelana. After sign up process
					is complete, sign in to visit conference page at Sammelana, and submit your paper for the conference.<br />
				</p>
			</div>
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="span2"><img src="/Data/img/signup/Peer-Reviewed-Papers.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Papers can be reviewed and marked by a reviewer after signing in. Each reviewer is usually appointed to review a given number
					of papers by the program chair. Reviewers are requested to consult the program chair regarding paper allocation to them.<br />
				</p>
			</div>
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="span2"><img src="/Data/img/signup/Event management pic.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Sammelana is a platform specially developed to ease the management of conference by providing intuitive ways to control and
					manage the activities of the conference. If you are a program chair, we hope you find managing your conference satisfying.<br />
				</p>
			</div>
		</div>
		<div class="span4 thumbnail sb-pad">
			<form id="CC" runat="server" action="/Pages/Conf/CreateConf.aspx" method="post">
				<fieldset id="CCStep1">
					<legend>New Conference - Basic Details</legend>
					<br />
					<h5>Conference Title</h5>
					<div><asp:TextBox ID="Ttl" runat="server" MaxLength="127" autofocus="autofocus" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCTtl1" class="alert sb-hdn">Conference Title not entered|Please enter the title of your conference here</div>
					<div id="CCTtl2" class="alert sb-hdn">Conference Title is invalid|The name of your conference contains invalid characters</div>
					<asp:Label ID="CCTtl" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Acronym</h5>
					<div><asp:TextBox ID="Acr" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAcr1" class="alert sb-hdn">Conference Acronym not entered|Please enter the unique acronym for your conference here</div>
					<div id="CCAcr2" class="alert sb-hdn">Conference Acronym entered is invalid|Please enter a valid Acronym for your conference</div>
					<div id="CCAcr3" class="alert sb-hdn">This Acronym has already been used|Please enter a unique acronym for your conference</div>
					<asp:Label ID="CCAcr" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Description</h5>
					<div><asp:TextBox ID="Dsc" runat="server" MaxLength="127" TextMode="MultiLine" Height="200px" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCDsc1" class="alert sb-hdn">Conference Description not entered|Please enter the description of your conference</div>
					<div id="CCDsc2" class="alert sb-hdn">Conference Description entered is invalid|Please remove special characters from your description</div>
					<asp:Label ID="CCDsc" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Upload Advertisement</h5>
					<div><asp:FileUpload ID="Inf" runat="server" CssClass="btn input-block-level" Width="100%" /></div>
					<asp:Label ID="CCInf" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<fieldset id="CCStep2">
					<h5>Management Password</h5>
					<div><asp:TextBox ID="Pwd" runat="server" MaxLength="127" TextMode="Password" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCPwd2" class="alert sb-hdn">Password too small|Please enter a password of length 6 characters or moree</div>
					<asp:Label ID="CCPwd" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Confirm Management Password</h5>
					<div><asp:TextBox ID="CPwd" runat="server" MaxLength="127" TextMode="Password" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCCPwd1" class="alert sb-hdn">Password not confirmed|Please confirm your account password here</div>
					<div id="CCCPwd2" class="alert sb-hdn">Passwords dont match|Please enter the same password here as above</div>
					<asp:Label ID="CCCPwd" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Category</h5>
					<div><asp:TextBox ID="Cat" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCCat1" class="alert sb-hdn">Conference category not selected|Please select an appropriate category for your conferencee</div>
					<div id="CCCat2" class="alert sb-hdn">Invalid conference category entered|Please enter a category without any special characters</div>
					<asp:Label ID="CCCat" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Begin Date</h5>
					<div><asp:TextBox ID="BgDt" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCBgDt1" class="alert sb-hdn">Conference begin date not specified|Please enter the begin date of your conference</div>
					<div id="CCBgDt2" class="alert sb-hdn">Invalid begin date entered|Please enter begin date in appropriate format</div>
					<asp:Label ID="CCBgDt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>Begin Time (local)</h5>
					<div><asp:TextBox ID="BgTm" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCBgTm1" class="alert sb-hdn">Conference begin time not specified|Please enter the begin time of your conference</div>
					<div id="CCBgTm2" class="alert sb-hdn">Invalid begin time entered|Please enter begin time in appropriate format</div>
					<asp:Label ID="CCBgTm" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>End Date</h5>
					<div><asp:TextBox ID="EdDt" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCEdDt1" class="alert sb-hdn">Conference end date not specified|Please enter the end date of your conference</div>
					<div id="CCEdDt2" class="alert sb-hdn">Invalid end date entered|Please enter end date in appropriate format</div>
					<asp:Label ID="CCEdDt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<h5>End Time (local)</h5>
					<div><asp:TextBox ID="EdTm" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCEdTm1" class="alert sb-hdn">Conference end time not specified|Please enter the end time of your conference</div>
					<div id="CCEdTm2" class="alert sb-hdn">Invalid end time entered|Please enter end time in appropriate format</div>
					<asp:Label ID="CCEdTm" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<fieldset id="CCStep3">
					<legend>New Conference - Location</legend>
					<br />
					<h5>At</h5>
					<div><asp:TextBox ID="AdAt" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAdAt2" class="alert sb-hdn">Invalid street address entered|Your street address contains invalid characters</div>
					<asp:Label ID="CCAdAt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Area</h5>
					<div><asp:TextBox ID="AdAr" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAdAr2" class="alert sb-hdn">Invalid area address entered|Your area address contains invalid characters</div>
					<asp:Label ID="CCAdAr" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>City</h5>
					<div><asp:TextBox ID="AdCt" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAdCt2" class="alert sb-hdn">Invalid city entered|Your city contains invalid characters</div>
					<asp:Label ID="CCAdCt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>State</h5>
					<div><asp:TextBox ID="AdSt" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAdSt2" class="alert sb-hdn">Invalid state entered|Your state contains invalid characters</div>
					<asp:Label ID="CCAdSt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Country</h5>
					<div><asp:TextBox ID="AdCn" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAdCn2" class="alert sb-hdn">Invalid country entered|Your country contains invalid characters</div>
					<asp:Label ID="CCAdCn" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>PIN Code</h5>
					<div><asp:TextBox ID="AdPn" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<div id="CCAdPn2" class="alert sb-hdn">Invalid PIN code entered|Your PIN code contains invalid characters</div>
					<asp:Label ID="CCAdPn" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<br /><br />
				<table class="sb-full sb-mid sb-btn">
				<tr>
					<td><asp:Button ID="Bck" runat="server" Text="Back" CssClass="btn btn-block btn-large" /></td>
					<td><asp:Button ID="Fwd" runat="server" Text="Continue" onclick="Sbmt_Click" CssClass="btn btn-block btn-large btn-primary" /></td>
				</tr>
				</table>
			</form>
		</div>
	</div>
</asp:Content>
