﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Vrfy.aspx.cs" Inherits="Sammelana_NITR.Pages.Cnf.Vrfy" %>
<asp:Content ID="Vrfy_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Vrfy_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<div>
	<form id="CV" runat="server" action="/Pages/Cnf/Vrfy.aspx">
		<div id="CV_Dft" runat="server">
			<div class="sb-form">
				<h4>We have encountered some error</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				You have requested an action which we cannot perform. It could probably be due to a broken or tampered link which brought you here.
				</p>
			</div>
		</div>
		<div id="CV_VrSnt" runat="server">
			<div class="sb-form">
				<h4>Conference Created - Awaiting Confirmation</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				The registration process for your conference is almost complete. However, this conference must be confirmed by our admin team.
				Once this conference gets confirmed, a mail will be sent to you. Confirmation process may take around 5 days. Hence, please be patient.
				</p>
				<p class="txt-hd" style="color: Gray;">
				A mail has also been sent to you regarding the creation of this conference.
				</p>
			</div>
		</div>
		<div id="CV_VrMl" runat="server">
			<div class="sb-form">
				<h4>Confirming Conference</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				Please wait while we confirm the conference ...
				</p>
			</div>
		</div>
		<div id="CV_VrDn" runat="server">
			<div class="sb-form">
				<h4>Conference Confirmed</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				Your email address has been verified and the sign up process has compeleted. You can now login to your account by clicking
				<a href="/Pages/Users/SignIn.aspx">here</a>.
				</p>
			</div>
		</div>
	</form>
	</div>
</asp:Content>
