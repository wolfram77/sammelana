﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sammelana_NITR.Core;
using System.Collections.Specialized;

namespace Sammelana_NITR.Pages.Tst
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Tst0 : System.Web.UI.Page
	{
		protected void Page_Load (object sender, EventArgs e)
		{
			object ret;
			sbDb db = new sbDb();
			ret = db.GetValue("Usr", "Eml", "Pwd = @Pwd", new sbKeyVals() { { "@Pwd", "123456" } });
			db.Disconnect();
		}
		protected void Sbmt0_Click(object sender, EventArgs e)
		{
		}
	}
}
