﻿<%@ Page Title="Internal Function Test 0 - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Tst0.aspx.cs" Inherits="Sammelana_NITR.Pages.Tst.Tst0" %>
<asp:Content ID="Tst0_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Tst0_Body" ContentPlaceHolderID="Main_Body" runat="server">
<form id="T0" runat="server">
	<fieldset>
		<legend><asp:Label ID="TstFn0" runat="server" Text="Test Function0"></asp:Label></legend>
		<br />
		<table>
			<tr>
			<td>Param 0</td>
			<td><asp:TextBox ID="Prm0" runat="server"></asp:TextBox></td>
			</tr>
			<tr>
			<td>Param 1</td>
			<td><asp:TextBox ID="Prm1" runat="server"></asp:TextBox></td>
			</tr>
			<tr>
			<td>Param 2</td>
			<td><asp:TextBox ID="Prm2" runat="server"></asp:TextBox></td>
			</tr>
			<tr>
			<td>Param 3</td>
			<td><asp:TextBox ID="Prm3" runat="server"></asp:TextBox></td>
			</tr>
			<tr>
			<td>Param 4</td>
			<td><asp:TextBox ID="Prm4" runat="server"></asp:TextBox></td>
			</tr>
			<tr>
			<td>Param 5</td>
			<td><asp:TextBox ID="Prm5" runat="server"></asp:TextBox></td>
		</table>
		<br /><br />
		<div><asp:Button ID="Sbmt0" runat="server" Text="Run Test Function0" onclick="Sbmt0_Click" CssClass="btn btn-large btn-primary" /></div>
		<br />
		<br />
		<br />
		<p>Output:</p>
		<div id="Out" runat="server"></div>
	</fieldset>
</form>
</asp:Content>
