﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Sammelana_NITR;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Sammelana_Main : System.Web.UI.MasterPage
	{
		// global variables
		// the last accessed user's login_id
		public static string LoginId = "";
		// user details and the details to be displayed
		sbKeyVals Usr; string Name, Pic;

		// Functions
		protected int SetHdr_Usr ()
		{
			if (Usr == null) return -1;
			Name = sbUsr.GetName(Usr);
			Pic = sbUsr.GetPic(Usr, false);
			HdrGst.Visible = false;
			UsrNm.Text = Name;
			UsrPic.ImageUrl = Pic;
			HdrUsr.Visible = true;
			return 0;
		}
		protected void SetHdr_Gst ()
		{
			HdrGst.Visible = true;
			HdrUsr.Visible = false;
		}

		// Event handlers
		protected void Page_Load (object sender, EventArgs e)
		{
			// is there a login_id
			HttpCookie hcLoginId = Request.Cookies["login_id"];
			string sLoginId = (hcLoginId == null) ? "" : hcLoginId.Value;
			// if no, then he is a guest user
			if (sLoginId == "") SetHdr_Gst();
			// if yes, then he is a signed in user
			else
			{
				try
				{
					if (sLoginId != LoginId)
					{
						sbDb db = new sbDb();
						Usr = sbUsr.GetSignedIn(db, sLoginId);
						db.Disconnect();
					}
					if (Usr != null) SetHdr_Usr();
					else SetHdr_Gst();
				}
				// if failed to obtain signed in info., then treat him
				// as a guest user
				catch (Exception)
				{
					SetHdr_Gst();
				}
			}
		}
		// need an automatic redirection facility (here)
	}
}