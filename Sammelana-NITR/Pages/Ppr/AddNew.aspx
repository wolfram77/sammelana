﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="Sammelana_NITR.Pages.Ppr.AddNew" %>
<asp:Content ID="AddNew_Head" ContentPlaceHolderID="Main_Head" runat="server">
	<title>Submit Paper - Sammelana</title>
	<script type="text/javascript">
	<!--
		// Global variables
		var sp_acc_vld = false;

		$(document).ready(function () {
			// Ensure Form validation
			$("#<%= SP_Ath.ClientID %>").focusout(function () { sbVldtFld("#<%= SP_Ath.ClientID %>", "email", "", "#SPEmail1", "#SPEmail2"); });
			$("#<%= SP.ClientID %>").submit(function () {
				// Validate Email and Password from Server
				if (cs_acc_vld) return true;
				var email = sbVldtFld("#<%= SP_Ath.ClientID %>", "email", "", "#SPEmail1", "#SPEmail2");
				if (email) {
					var v_eml = $("#<%= SP_Ath.ClientID %>").val();
					$.post("/Pages/Users/ChangePassword.aspx", { Eml: v_eml }, function (resp) {
						if (resp != "") { $("#CDEmail3").show(vldtrUpdate); cs_acc_vld = false; }
						else { cs_acc_vld = true; $("#<%= SP_Sbmt.ClientID %>").click(); }
					});
				}
				return false;
			});
		});
	-->
	</script>
</asp:Content>
<asp:Content ID="AddNew_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<h2 class="sb-mid" style="padding-bottom: 24px;">Submit Paper to Conference</h2>
	<p class="sb-form sb-cntr" style="font-size: 13px; text-align: justify; width: 400px; margin-bottom: 40px;">
		Please enter all the details carefully before submitting your paper. We wish you all the best.
	</p>
	<div class="sb-cntr sb-form" style="width: 400px;">
		<form id="SP" runat="server" action="/Pages/Users/SignIn.aspx" method="post">
			<h3>Paper Title</h3>
			<p><asp:TextBox ID="SP_Ttl" runat="server" MaxLength="127" Width="100%" autofocus="autofocus"></asp:TextBox></p>
			<div id="SPTtl1" class="sb-error">(no email-id entered)</div>
			<div id="SPTtl2" class="sb-error">(invalid email-id entered)</div>
			<div id="SPTtl3" class="sb-error">(email or password is invalid)</div>
			<h3>Abstract</h3>
			<p><asp:TextBox ID="SP_Abs" runat="server" MaxLength="127" Width="100%" TextMode="MultiLine" Height="200px"></asp:TextBox></p>
			<div id="SPPbDsc1" class="sb-error">(no email-id entered)</div>
			<div id="SPPbDsc2" class="sb-error">(invalid email-id entered)</div>
			<h3>Keywords</h3>
			<p><asp:TextBox ID="SP_Kyw" runat="server" MaxLength="127" Width="100%" TextMode="MultiLine" Height="200px"></asp:TextBox></p>
			<div id="Div3" class="sb-error">(no email-id entered)</div>
			<div id="Div4" class="sb-error">(invalid email-id entered)</div>
			<h3>Authors</h3>
			<p><asp:TextBox ID="SP_Ath" runat="server" MaxLength="127" Width="100%" TextMode="MultiLine" Height="200px"></asp:TextBox></p>
			<div id="SPPbTtl1" class="sb-error">(no email-id entered)</div>
			<div id="SPPbTtl2" class="sb-error">(invalid email-id entered)</div>
			<h3>Upload Paper</h3>
			<p><asp:FileUpload ID="SP_Doc" runat="server" /></p>
			<div id="Div1" class="sb-error">(no email-id entered)</div>
			<div id="Div2" class="sb-error">(invalid email-id entered)</div>
			<p style="padding-top: 32px;"><asp:Button ID="SP_Sbmt" runat="server" Text="Submit Paper" data-theme="e" onclick="SP_Sbmt_Click" /></p>
		</form>
		<hr />
		<div class="sb-mid" data-mini="true" style="margin-top: 24px;">
			<p><a style="font-size: 12px;" href="/Pages/Users/SignIn.aspx">Need to Sign in?</a></p>
			<p><a style="font-size: 12px;" href="/Pages/Users/ChangePassword.aspx">Forgot Password?</a></p>
			<p><a style="font-size: 12px;" href="/Pages/Users/SignUp.aspx">Don't have an account?</a></p>
		</div>
	</div>
</asp:Content>
