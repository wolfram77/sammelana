﻿<%@ Page Title="Suggest a feature - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Sgst.aspx.cs" Inherits="Sammelana_NITR.Pages.Cmn.Sgst" %>
<asp:Content ID="SF_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="SF_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<script type="text/javascript">
	<!--
		$(document).ready(function () {
			// Ensure Form validation
			$("#<%= SF_Email.ClientID %>").focusout(function () { sbVldtFld("#<%= SF_Email.ClientID %>", "email", "", "#SFEmail1", "#SFEmail2"); });
			$("#<%= SF_Name.ClientID %>").focusout(function () { sbVldtFld("#<%= SF_Name.ClientID %>", "name", "", "#SFName1", "#SFName2"); });
			$("#<%= SF_Sgst.ClientID %>").focusout(function () { sbVldtFld("#<%= SF_Sgst.ClientID %>", "address", "", "#SFSgst1", "#SFSgst2"); });
			$("#<%= SF.ClientID %>").submit(function () {
				// Validate Fields
				var email = sbVldtFld("#<%= SF_Email.ClientID %>", "email", "", "#SFEmail1", "#SFEmail2");
				var name = sbVldtFld("#<%= SF_Name.ClientID %>", "name", "", "#SFName1", "#SFName2");
				var sgst = sbVldtFld("#<%= SF_Sgst.ClientID %>", "address", "", "#SFSgst1", "#SFSgst2");
				if (email && name && sgst) return true;
				return false;
			});
		});
	-->
	</script>
	<div class="thumbnails">
		<div class="span4 thumbnail sb-pad">
			<form id="SF" runat="server" action="/Pages/Mixed/SuggestFeature.aspx" method="post">
				<fieldset>
				<legend>Provide Suggestion</legend>
				<br />
				<h5>Email</h5>
				<div><asp:TextBox ID="Eml" runat="server" MaxLength="127" autofocus="autofocus" CssClass="input-block-level"></asp:TextBox></div>
				<div id="SFEml1" class="alert sb-hdn">No email-id entered|Please enter your email-id here so that we can contact you for more information</div>
				<div id="SFEml2" class="alert sb-hdn">Invalid email-id entered|Please enter a valid email-id here</div>
				<asp:Label ID="SFEml" runat="server" CssClass="alert sb-hdn"></asp:Label>
				<br />
				<h5>Full Name</h5>
				<div><asp:TextBox ID="Nm" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
				<div id="SFNm1" class="alert sb-hdn">No name entered|Please enter your name so that we get to know you</div>
				<div id="SFNm2" class="alert sb-hdn">Invalid name entered|Please enter a valid name here</div>
				<asp:Label ID="SFNm" runat="server" CssClass="alert sb-hdn"></asp:Label>
				<br />
				<h5>Suggestion</h5>
				<div><asp:TextBox ID="Txt" runat="server" TextMode="MultiLine" CssClass="input-block-level" Rows="6"></asp:TextBox></div>
				<div id="SFTxt1" class="alert sb-hdn">No suggestion entered|Please enter your suggestion here</div>
				<div id="SFTxt2" class="alert sb-hdn">Your suggestion contains invalid characters|Please remove invalid letters, such as quotation marks</div>
				<asp:Label ID="SFTxt" runat="server" CssClass="alert sb-hdn"></asp:Label>
				<br /><br />
				<div><asp:Button ID="Sbmt" runat="server" Text="Make Suggestion" CssClass="btn btn-block btn-large btn-primary" onclick="Sbmt_Click" /></div>
				</fieldset>
			</form>
			<hr />
			<div class="sb-mid">
				<p><a class="sb-txt-med" href="/Pages/Users/SignUp.aspx">Don't have an account? Sign Up</a></p>
				<p><a class="sb-txt-med" href="/Pages/FAQ.aspx">Facing some Problem? Goto FAQ</a></p>
				<p><a class="sb-txt-med" href="/Pages/Mixed/ContactSupport.aspx">Some other problem? Contact us</a></p>
			</div>
		</div>
		<div class="span7">
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="offset1 span2"><img src="/Data/img/signin/work_hard.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Far and away the best prize that life has to offer is the chance to work hard at work worth doing.<br />
					<br />
					<em>- Theodore Roosevelt</em>
				</p>
			</div>
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="offset1 span2"><img src="/Data/img/signin/innovation.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Innovation has nothing to do with how many R&D dollars you have. When Apple came up with the Mac, IBM was spending at least 100 times more on R&D. It's not about money. It's about the people you have, how you're led, and how much you get it.<br />
					<br />
					<em>- Steve Jobs</em>
				</p>
			</div>
		</div>
	</div>
</asp:Content>
