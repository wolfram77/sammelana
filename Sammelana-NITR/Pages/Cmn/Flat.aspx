﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Flat.Master" AutoEventWireup="true" CodeBehind="Flat.aspx.cs" Inherits="Sammelana_NITR.Pages.Cmn.Flat" %>
<asp:Content ID="Flatx_Head" ContentPlaceHolderID="Flat_Head" runat="server">
</asp:Content>
<asp:Content ID="Flatx_Body" ContentPlaceHolderID="Flat_Body" runat="server">
	<div class="sb-pad sb-mid">
		<div class="thumbnail sb-pad">
            <div id="Dsclmr" runat="server">
			    <h3 class="MsgHdr">Disclaimer</h3>
			    <br />
			    <p class="MsgTxt">
				    This website has been developed by National Institute of Technology, Rourkela. Content on this website is compiled, 
				    maintained by NIT, Rourkela. This website is hosted by NIT, Rourkela.
				    Though all efforts have been made to keep the content on this website accurate and up-to-date, the same should not 
				    be construed as a statement of law or used for any legal purposes. All queries regarding the content of this website 
				    may be directed to Registrar, National Institute of Technology Rourkela, Rourkela 769008 or registrar@nitrkl.ac.in
			    </p>
            </div>
           <div id="Cpyrght" runat="server">
			    <h3 class="MsgHdr">Copyright</h3>
			    <br />
			    <p class="MsgTxt">
                    Material featured on this site may be reproduced free of charge in any format or media without requiring 
                    specific permission. This is subject to the material being reproduced accurately and not being used in a 
                    derogatory manner or in a misleading context. Where the material is being published or issued to others, 
                    the source must be prominently acknowledged.

                    However, the permission to reproduce this material does not extend to any material on this site which is 
                    identified as being the copyright of a third party. Authorisation to reproduce such material must be obtained 
                    from the copyright holders concerned.			
			    </p>
            </div>
           <div id="PrvcyStmnt" runat="server">
			    <h3 class="MsgHdr">Privacy Statement</h3>
			    <br />
			    <p class="MsgTxt">
                    As a general rule, this web site does not collect Personal Information about you when you visit the site. 
                    You can generally visit the site without revealing Personal Information, unless you choose to provide such 
                    information. Any Personal information collected shall be used only for the stated purpose and shall NOT be 
                    shared with any other department/ organization (public/private).

                    This site may contain links to non-Government sites whose data protection and privacy practices may differ 
                    from ours. We are not responsible for the content and privacy practices of these other websites and encourage 
                    you to consult the privacy notices of those sites.            
			    </p>
            </div>
		</div>
	</div>
</asp:Content>
