﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Accs.aspx.cs" Inherits="Sammelana_NITR.Pages.Cmn.Accs" %>
<asp:Content ID="Accs_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Accs_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<div class="hero-unit">
		<h2>Sorry! This page cannot be accessed directly.</h2>
		<br />
		<p>
			Incomplete or broken links could cause you to reach this page. However, if you believe that the link provided to you is valid and is
			required by you essentially, then please contact us. 
		</p>
		<br />
		<a href="/Pages/Mixed/ContactSupport.aspx" class="btn btn-large btn-warning">Contact Support</a>
	</div>
	<div class="thumbnail sb-pad sb-mid">
		<p class="sb-hdr-med">
			Please visit the main website for all options and updates
		</p>
		<br />
		<a href="/Default.aspx" class="btn btn-primary">Visit main website</a>
	</div>
</asp:Content>
