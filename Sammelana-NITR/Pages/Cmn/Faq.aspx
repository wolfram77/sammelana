﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Faq.aspx.cs" Inherits="Sammelana_NITR.Pages.Cmn.Faq" %>
<asp:Content ID="Faq_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Faq_Body" ContentPlaceHolderID="Main_Body" runat="server">
<div class="sb-txt-med">
<br />
<em>This page contains basic facts about Sammelana.</em>
<br />
<br />
<div class="thumbnail sb-pad">
	<dl>
		<dt>Is Sammelana free?</dt>
		<dd>Is Sammelana free as in freedom or free as in beer? We are free as Google search. This means that we do not charge for our main product (conference management) and only charge for additional services (for example, helpdesk). You are free (as in freedom) to choose either free (as in free beer) use of Sammelana or paid-for additional services.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Installation</h4>
	<dl>
		<dt>Can I install Sammelana on my server?</dt>
		<dd>No. Sammelana is a Web service. We host it on a high-performance commercial server.</dd>
		<br />
		<dt>We want to have data on our server, should we use another system?</dt>
		<dd>You can try to find another system, however we do not recommend such a solution for several reasons. If you do this, then backup, recovery, security, performance, availability and many other issues handled smoothly by Sammelana will become your problem. This means you can be in a situation to lose your data, have a system unavailable at the most critical time, or simply malfunctioning. You may lose your data, time and reputation as a result. Note also that we have a paid-for service making it possible for you to make copies of all your conference data in the Excel and XML format at any time.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Support</h4>
	<dl>
		<dt>I heard that other conference management systems provide user support, while Sammelana comes with no support. Is this true?</dt>
		<dd>No. We provide no free support. No conference system provides free support. We do provide paid-for support in addition to the free version.</dd>
		<br />
		<dt>What level of support is included in the standard free installation?</dt>
		<dd>We help you if you have troubles to install your conference. We maintain, update and upgrade Sammelana on a regular basis. We make backup of your data. We provide high-speed networking with over 99.9% uptime. We provide bug fixes. We try to maintain online Help pages. For anything else you should apply for paid-for services.</dd>
		<br />
		<dt>Can my conference have additional support?</dt>
		<dd>Yes. We provide a helpdesk support, but it is not free. You should apply for it separately. Note that our prices are cheaper than those of our competitors: ask for a quote!</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Efficiency</h4>
	<dl>
		<dt>Is Sammelana fast enough to handle large conferences and program committee meetings?</dt>
		<dd>Yes. We use an original implementation technique which is dozens of times faster than implementations of Web services using the standard PHP/MySQL technology. We never had complaints about performance and are confident we can handle a conference of any size.</dd>
		<br />
		<dt>Can Sammelana work with large submissions?</dt>
		<dd>Yes. We are using a 1Gbs network. This means that we are able to accept over 300 standard size submissions per second. This also means we will be able to deal with conferences dealing with very large submissions, such as videos.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Non-English version</h4>
	<dl>
		<dt>Can we use Sammelana in languages different from English?</dt>
		<dd>Yes and no. The information Sammelana stores is stored in Unicode, so it can be in any language, for example, Chinese or French. However, all instructions in Sammelana pages are in English. In November 2011 we introduced a new service, where one can customize the submission page, so that it makes it easier for your authors to follow your submission procedure.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Customization</h4>
	<dl>
		<dt>Can Sammelana be customized?</dt>
		<dd>Sammelana is highly customizable. It has a number of options allowing conference organizers to choose a model suitable for their conference. These options include submission, reviewing and proceedings creation. When a new conference is created, it is immediately ready to accept submissions using a standard configuration. This configuration can be changed by the conference administrators (chairs) at any time.</dd>
		<br />
		<dt>Sammelana does not have some features our conference needs, what should we do?</dt>
		<dd>Contact us. We might be able to implement what you need. We have a wish list of Sammelana enhancements and improvements, and often rearrange our priorities, especially when this is required by our premium customers.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Reliability and security</h4>
	<dl>
		<dt>Is Sammelana available 24/7?</dt>
		<dd>Yes. Sammelana is running 24/7/365. Since 2005 we had only two interrupts. In one case we had a total hard disk failure. Sammelana was up running three hours later with no data lost. The second interrupt was 6 hours long, when our server provided had to do some work on its servers. Sometimes we have updates of Sammelana. Normally, they are running a few seconds or a few minutes. If we have an update likely to run more than 5 minutes, we normally perform it on Saturdays, when the number of users is low.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Backup and recovery</h4>
	<dl>
		<dt>We are afraid to lose our conference data. What are your backup and recovery procedures?</dt>
		<dd>We use the technique known as database replication to keep your data safe. We have live replicas of all data on two backup servers different from the Sammelana main server. This means that, in the case we have a total server crash, we will be able to recover data using one of the backup servers. For a peace of mind you can also use our paid-for service allowing you to download copies of all your conference data.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Registration and payment</h4>
	<dl>
		<dt>Can Sammelana handle attendee registration and online payment?</dt>
		<dd>We have registration and payment facilities implemented. They are now being used with several conferences and will be released for other conferences soon.</dd>
		<br />
	</dl>
</div>
<div class="thumbnail sb-pad">
	<h4>Other</h4>
	<dl>
		<dt>How often are new versions of Sammelana released?</dt>
		<dd>We use the agile programming technology, with no major releases. We made about 550 updates in 2011. It means it is not unusual for us to have several updates a day. Our updates are normally made without stopping Sammelana.</dd>
		<br />
	</dl>
</div>
</div>
</asp:Content>
