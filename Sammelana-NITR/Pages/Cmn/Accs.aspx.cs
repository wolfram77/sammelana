﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Cmn
{
	public partial class Accs : System.Web.UI.Page
	{
		string rqAttr, rqDat;

		protected void Page_Load (object sender, EventArgs e)
		{
			// Get query requests
			rqAttr = Request["attr"];
			rqDat = Request["dat"];

			// display default error page on no query
			if (rqAttr == null || rqAttr == "") return;

			// perform action depending upon the query
			switch (rqAttr)
			{
				default:
					sbHtml.SendFile(Response, sbStore.GetFullPath(rqAttr, rqDat, false));
					break;
			}
		}
	}
}