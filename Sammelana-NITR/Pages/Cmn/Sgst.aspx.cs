﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Cmn
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Sgst : System.Web.UI.Page
	{
		// Form Data is stored here
		sbKeyVals Sgt;

		// Functions
		// gets all the info entered by user into Sgst keyvals (feature suggestion)
		protected void GetSgtInfo()
		{
			Sgt = new sbKeyVals();
			// fill in with all the details
			Sgt["SgId"] = 0;
			Sgt["Eml"] = Eml.Text;
			Sgt["Nm"] = Nm.Text;
			Sgt["Txt"] = Txt.Text;
		}
		// validate all user information saved into Sgst keyvals and remove the unrequired fields
		protected bool VldtSgtInfo(sbDb db)
		{
			// assume the entered info to be valid first
			bool valid = true;

			// validate all fields normally
			valid = (sbForm.VldtFldAuto(Eml.Text, "email", "", SFEml)) ? valid : false;
			valid = (sbForm.VldtFldAuto(Nm.Text, "name", "", SFNm)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Txt.Text, "address", "", SFTxt)) ? valid : false;
			// return whether all of the entered info is valid or not
			return valid;
		}

		// Event handlers
		protected void Page_Load(object sender, EventArgs e)
		{
		}
		protected void Sbmt_Click(object sender, EventArgs e)
		{
			int ret;

			sbDb db = new sbDb();
			GetSgtInfo();
			if (VldtSgtInfo(db))
			{
				ret = sbSgt.AddNew(db, Sgt);
				if (ret == 0) Response.Redirect("/Pages/Users/EmailVerify.aspx?md=vrsnt");
				else Response.Redirect("/Pages/Users/EmailVerify.aspx");
			}
			db.Disconnect();
		}
	}
}