﻿<%@ Page Title="Contact Support - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Spprt.aspx.cs" Inherits="Sammelana_NITR.Pages.Spprt" %>
<asp:Content ID="CntctSpprt_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="CntctSpprt_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<script type="text/javascript">
	<!--
		$(document).ready(function () {
			// Ensure Form validation
			$("#<%= CS_Email.ClientID %>").focusout(function () { sbVldtFld("#<%= CS_Email.ClientID %>", "email", "", "#CSEmail1", "#CSEmail2"); });
			$("#<%= CS_Name.ClientID %>").focusout(function () { sbVldtFld("#<%= CS_Name.ClientID %>", "name", "", "#CSName1", "#CSName2"); });
			$("#<%= CS_PbTtl.ClientID %>").focusout(function () { sbVldtFld("#<%= CS_PbTtl.ClientID %>", "address", "", "#CSPbTtl1", "#CSPbTtl2"); });
			$("#<%= CS_PbDsc.ClientID %>").focusout(function () { sbVldtFld("#<%= CS_PbDsc.ClientID %>", "address", "", "#CSPbDsc1", "#CSPbDsc2"); });
			$("#<%= CS.ClientID %>").submit(function () {
				// Validate Form fields
				var email = sbVldtFld("#<%= CS_Email.ClientID %>", "email", "", "#CSEmail1", "#CSEmail2");
				var name = sbVldtFld("#<%= CS_Name.ClientID %>", "name", "", "#CSName1", "#CSName2");
				var pbttl = sbVldtFld("#<%= CS_PbTtl.ClientID %>", "address", "", "#CSPbTtl1", "#CSPbTtl2");
				var pbdsc = sbVldtFld("#<%= CS_PbDsc.ClientID %>", "address", "", "#CSPbDsc1", "#CSPbDsc2");
				if (email && name && pbttl && pbdsc) return true;
				return false;
			});
		});
	-->
	</script>
	<div class="thumbnails">
		<div class="span4 thumbnail sb-pad">
			<form id="CS" runat="server" action="/Pages/Mixed/ContactSupport.aspx" method="post">
				<fieldset>
				<legend>Contact for Support</legend>
				<br />
				<h5>Email</h5>
				<div><asp:TextBox ID="CS_Email" runat="server" MaxLength="127" autofocus="autofocus" CssClass="input-block-level"></asp:TextBox></div>
				<div id="CSEmail1" class="alert sb-hdn">No email-id entered|Please enter your email-id here</div>
				<div id="CSEmail2" class="alert sb-hdn">Invalid email-id entered|Please enter a valid email-id here</div>
				<br />
				<h5>Full Name</h5>
				<div><asp:TextBox ID="CS_Name" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
				<div id="CSName1" class="alert sb-hdn">No name entered|Please enter your name so that we get to know you</div>
				<div id="CSName2" class="alert sb-hdn">Invalid name entered|Please enter a valid name here</div>
				<br />
				<h5>Problem Title</h5>
				<div><asp:TextBox ID="CS_PbTtl" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
				<div id="CSPbTtl1" class="alert sb-hdn">No problem title entered|Please enter your problem title here</div>
				<div id="CSPbTtl2" class="alert sb-hdn">Your problem title contains invalid characters|Please remove invalid letters, such as quotation marks</div>
				<br />
				<h5>Problem Description</h5>
				<div><asp:TextBox ID="CS_PbDsc" runat="server" MaxLength="127" TextMode="MultiLine" CssClass="input-block-level" Rows="5"></asp:TextBox></div>
				<div id="CSPbDsc1" class="alert sb-hdn">No problem description entered|Please enter your problem description here</div>
				<div id="CSPbDsc2" class="alert sb-hdn">Your problem description contains invalid characters|Please remove invalid letters, such as quotation marks</div>
				<br /><br />
				<div><asp:Button ID="CS_Sbmt" runat="server" Text="Submit Problem" onclick="CS_Sbmt_Click" CssClass="btn btn-block btn-large btn-primary" /></div>
				</fieldset>
			</form>
			<hr />
			<div class="sb-mid">
				<p><a href="/Pages/Users/ChangePassword.aspx">Forgot Password?</a></p>
				<p><a href="/Pages/Users/SignUp.aspx">Don't have an account?</a></p>
				<p><a href="/Pages/Mixed/ContactSupport.aspx">Some other problem? Contact us</a></p>
			</div>
		</div>
		<div class="span7">
			<h3 class="sb-mid sb-pad">Contact for Support</h3>
			<br />
			<p>
				Sammelana is an online conference management system which provides conference organizers and all the people involved in the conference, 
				a portal, where they can manage their contributions to the conference in an easy and efficient way. It mainly focuses on handling conference 
				papers starting from allowing an author to submit his / her paper to the conference, to publishing the list of accepted papers and reporting 
				the results to the authors. It has been designed, keeping usability and ease of access in mind and provides intuitively simple UI for controlling 
				all the activities of a conference. It is developed and managed by National Institute of Technology, Rourkela.
			</p>
		</div>
	</div>
</asp:Content>
