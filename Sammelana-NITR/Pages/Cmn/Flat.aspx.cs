﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Cmn
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Flat : System.Web.UI.Page
	{
		// the requested parameters
		sbKeyVals ReqMsg;

		// Functions
		// get the request message from script or url query
		protected sbMsgMode GetReqMsg()
		{
			// was a script message recieved?
			ReqMsg = sbHtml.RecvMsg(Request);
			if (ReqMsg.Count > 0) return sbMsgMode.ScriptMsg;
			// was a url query recieved?
			ReqMsg = sbKv.ToKeyVals(Request.QueryString);
			return sbMsgMode.Query;
		}
		// initialize the page
		protected void InitPage()
		{
			Dsclmr.Visible = false;
			Cpyrght.Visible = false;
			PrvcyStmnt.Visible = false;
		}

		// Event handlers
		protected void Page_Load(object sender, EventArgs e)
		{
			InitPage();
			GetReqMsg();
			if (ReqMsg.Count == 0) return;
			switch (sbKv.GetStrVal(ReqMsg, "Item"))
			{
				case "Dsclmr":
					Dsclmr.Visible = true;
					break;
				case "Cpyrght":
					Cpyrght.Visible = true;
					break;
				case "PrvcyStmnt":
					PrvcyStmnt.Visible = true;
					break;
			}
		}
	}
}