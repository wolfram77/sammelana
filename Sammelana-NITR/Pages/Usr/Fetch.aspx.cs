﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Usr
{
	// Global Definations
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Fetch : System.Web.UI.Page
	{
		// Global variables
		string wbReq, wbEml, wbPwd;

		// Functions
		// send respone to script whether password is correct or not
		protected bool Msg_UsrSgnIn(string Eml, string Pwd)
		{
			sbRes ret;

			sbDb db = new sbDb();
			// for matching password, send "", else send "incorrect"
			ret = sbUsr.MatchCred(db, Eml, Pwd);
			if (ret == sbRes.Ok) sbHtml.SendRawMsg(Response, "");
			else sbHtml.SendRawMsg(Response, "incorrect");
			db.Disconnect();
			return (ret == sbRes.Ok)? true : false;
		}
		// allow user to login to sammelana by creaing a sign in action, and
		// sending the action id as a login cookie (login_id)
		protected bool UsrSgnIn(string Eml, string Pwd)
		{
			int UId;
			Guid AId;
			sbRes ret;

			sbDb db = new sbDb();
			ret = sbUsr.MatchCred(db, Eml, Pwd);
			if(ret == sbRes.Ok)
			{
				// add sign in action
				UId = (int) db.GetValue("Usr", "UId", "Eml = @Eml", "@Eml", Eml);
				AId = sbAct.AddNew(db, UId, "UsrSgnIn", sbUsr.SgnInTm);
				if (AId == Guid.Empty) return false;
				// send the action id to the user browser as cookie
				sbHtml.SetCookie(Request, Response, "login_id", AId, sbUsr.SgnInTm);
				Response.Redirect("/Pages/Usr/Home.aspx");
			}
			db.Disconnect();
			return (ret == sbRes.Ok)? true : false;
		}

		// Event Handlers
		protected void Page_Load (object sender, EventArgs e)
		{
			Guid AId;
			string login_id;
			HttpCookie hlogin_id;

			wbReq = Request["Req"];
			wbReq = (wbReq == null)? "" : wbReq;
			wbEml = Request["Eml"];
			wbEml = (wbEml == null)? "" : wbEml;
			wbPwd = Request["Pwd"];
			wbPwd = (wbPwd == null)? "" : wbPwd;
			switch (wbReq)
			{
				// check sign in check request from script
				case "sbChkEmail":
					Msg_UsrSgnIn(wbEml, wbPwd);
					break;
				// if user is already signed in, then redirect him to home
				default:
					hlogin_id = Request.Cookies["login_id"];
					login_id = (hlogin_id != null)? hlogin_id.Value : "";
					AId = sbForm.GetGuid(login_id);
					if (AId != Guid.Empty) Response.Redirect("/Pages/Usr/Home.aspx");
					break;
			}
		}
		protected void Sbmt_Click(object sender, EventArgs e)
		{
			wbEml = Eml.Text;
			wbPwd = Pwd.Text;
			UsrSgnIn(wbEml, wbPwd);
		}
	}
}
