﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Usr
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class AddNew : System.Web.UI.Page
	{
		// Form Data is stored here
		sbKeyVals Usr;
		byte[] UsrPicData, UsrCVdata;
		// Request Data
		string rqReq, rqEml, rqPwd;

		// Functions
		// reply to an URL query from sign up page
		protected void Msg_SgnUpChk(sbDb db)
		{
			string msg = "";

			sbRes emlStat = sbUsr.MatchCred(db, rqEml, rqPwd);
			// generate reply string according to email status
			switch (emlStat)
			{
				case sbRes.NoAvail:
					msg = "";
					break;
				case sbRes.Ok:
					msg = "Account already signed up|You should log in. Credentials provided match with an existing account";
					break;
				case sbRes.No:
					msg = "Account already signed up|This email-id has already been used for an account. If this is your email-id then please proceed to sign in";
					break;
			}
			// send the response (sign up page script will then display the sent message in popover)
			sbHtml.SendRawMsg(Response, msg);
		}
		// gets all the info entered by user into Usr keyvals (sign up)
		protected void GetUsrInfo ()
		{
			Usr = new sbKeyVals();
			// fill in with all the details
			Usr["UId"] = 0;
			Usr["Eml"] = Eml.Text;
			Usr["Pwd"] = Pwd.Text;
			Usr["AcVf"] = false;
			Usr["FtNm"] = FtNm.Text;
			Usr["MdNm"] = MdNm.Text;
			Usr["LtNm"] = LtNm.Text;
			Usr["Gndr"] = sbForm.GetGender(Gndr.SelectedIndex);
			Usr["DoB"] = sbForm.GetDateTime(DoB.Text);
			Usr["Pic"] = Pic.FileName;
			UsrPicData = Pic.FileBytes;
			Usr["Pos"] = Pos.Text;
			Usr["Org"] = Org.Text;
			Usr["WrkH"] = WrkH.Text;
			Usr["CV"] = CV.FileName;
			UsrCVdata = CV.FileBytes;
			Usr["Noff"] = Noff.Text;
			Usr["Nhom"] = Nhom.Text;
			Usr["Nmob"] = Nmob.Text;
			Usr["Nfax"] = Nfax.Text;
			Usr["AdAt"] = AdAt.Text;
			Usr["AdAr"] = AdAr.Text;
			Usr["AdCt"] = AdCt.Text;
			Usr["AdSt"] = AdSt.Text;
			Usr["AdCn"] = AdCn.Text;
			Usr["AdPn"] = AdPn.Text;
		}
		// validate all user information saved into Usr keyvals and remove the unrequired fields
		protected bool VldtUsrInfo (sbDb db)
		{
			// assume the user info to be valid first
			bool valid = true;

			// perform thorough email validity check
			if(!sbForm.VldtFldAuto(Eml.Text, "email", "", SUEml)) valid = false;
			else
			{
				sbRes emlVld = sbUsr.MatchCred(db, Eml.Text, Pwd.Text);
				if (emlVld == sbRes.Ok)
				{
					valid = false;
					sbForm.VldtrReport(SUEml, "This email-id is already registered");
				}
			}
			// validate all other fields normally
			valid = (sbForm.VldtFld(Pwd.Text, "pwd", "", SUPwd, "Enter password here", "Password must be 6 characters or more"))? valid : false;
			valid = (sbForm.VldtFld(CPwd.Text, "mtch", Pwd.Text, SUCPwd, "Confirm password here", "Passwords dont match")) ? valid : false;
			valid = (sbForm.VldtFldAuto(FtNm.Text, "nm", "", SUFtNm)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(MdNm.Text, "nm", "", SUMdNm)) ? valid : false;
			valid = (sbForm.VldtFldAuto(LtNm.Text, "nm", "", SULtNm)) ? valid : false;
			valid = (sbForm.VldtFldAuto(DoB.Text, "date", "", SUDoB)) ? valid : false;
			valid = (sbForm.VldtFldAuto(Gndr.Text, "gndr", "", SUGndr)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Pic.FileName, "pic", UsrPicData.Length, SUPic)) ? valid : false;
			valid = (sbForm.VldtFldAuto(Pos.Text, "addr", "", SUPos)) ? valid : false;
			valid = (sbForm.VldtFldAuto(Org.Text, "addr", "", SUOrg)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(WrkH.Text, "addr", "", SUWrkH)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(CV.FileName, "doc", UsrCVdata.Length, SUCV)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Noff.Text, "phon", "", SUNoff)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Nhom.Text, "phon", "", SUNhom)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Nmob.Text, "phon", "", SUNmob)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(Nfax.Text, "phon", "", SUNfax)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdAt.Text, "addr", "", SUAdAt)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdAr.Text, "addr", "", SUAdAr)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdCt.Text, "addr", "", SUAdCt)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdSt.Text, "addr", "", SUAdSt)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdCn.Text, "addr", "", SUAdCn)) ? valid : false;
			valid = (sbForm.VldtFldAutoInv(AdPn.Text, "addr", "", SUAdPn)) ? valid : false;
			// return whether all of the entered info is valid or not
			return valid;
		}

		// Event Handlers
		protected void Page_Load (object sender, EventArgs e)
		{
			Guid AId;
			string login_id;
			HttpCookie hlogin_id;

			rqReq = Request["Req"];
			rqReq = (rqReq == null) ? "" : rqReq;
			// reply to check email request from script
			if (rqReq == "SgnUpChk")
			{
				rqEml = Request["Eml"];
				rqEml = (rqEml == null) ? "" : rqEml;
				rqPwd = Request["Pwd"];
				rqPwd = (rqPwd == null) ? "" : rqPwd;
				sbDb db = new sbDb();
				Msg_SgnUpChk(db);
				db.Disconnect();
			}
			// is user already signed in, redirect him to home
			else
			{
				hlogin_id = Request.Cookies["login_id"];
				login_id = (hlogin_id != null) ? hlogin_id.Value : "";
				AId = sbForm.GetGuid(login_id);
				if (AId != Guid.Empty) Response.Redirect("/Pages/Users/Home.aspx");
			}
		}
		protected void Sbmt_Click (object sender, EventArgs e)
		{
			int ret;

			sbDb db = new sbDb();
			GetUsrInfo();
			if(VldtUsrInfo(db))
			{
				ret = sbUsr.AddNew(db, Usr, UsrPicData, UsrCVdata);
				if (ret == 0) Response.Redirect("/Pages/Users/EmailVerify.aspx?md=vrsnt");
				else Response.Redirect("/Pages/Users/EmailVerify.aspx");
			}
			db.Disconnect();
		}
	}
}
