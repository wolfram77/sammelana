﻿<%@ Page Title="Sign In - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Fetch.aspx.cs" Inherits="Sammelana_NITR.Pages.Usr.Fetch" %>
<asp:Content ID="Fetch_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Fetch_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<script type="text/javascript">
	<!--
		// interactive Eml & Pwd check
		var oEml = "", oPwd = "", oVld = false;
		function ChkEmlPwd() {
			var eml = $("#<%= Eml.ClientID %>").val();
			var pwd = $("#<%= Pwd.ClientID %>").val();
			if (pwd.length == 0 || (oVld && oEml === eml && oPwd === pwd)) return true;
			oEml = eml; oPwd = pwd;
			$.post("/Pages/Usr/Fetch.aspx", { "Req": "sbChkEmail", "Eml": eml, "Pwd": pwd }, function (resp) {
				if (resp === "" || resp === null) { oVld = true; $("#<%= Sbmt.ClientID %>").click(); }
			});
			return false;
		}

		$(document).ready(function () {
			sbForm_AddVldtn({
				"Nm": "#<%= F.ClientID %>",
				"Sbmt": { "Nm": "#<%= Sbmt.ClientID %>", "LtTxt": "Sign In", "LtCls": "btn-primary" },
				"Prev": {},
				"Blk": [
					{ "Nm": "#F1", "Fld": [
							{ "Nm": "#<%= Eml.ClientID %>", "Typ": "Eml", "Fn": ChkEmlPwd, "MsgBlnk": "No email-id entered|Please enter your email-id here", "MsgInv": "Invalid email-id entered|Please enter a valid email-id here", "MsgFn": "Sign In failed!|Invalid Sign In details entered. Please enter correct email-address and password, and then try again." },
							{ "Nm": "#<%= Pwd.ClientID %>", "Typ": "Pwd", "Fn": ChkEmlPwd, "MsgBlnk": "No password entered|Please enter your account password here", "MsgInv": "Password too small|Password must be of atleast 6 characters" }
						]
					}
				]
			});
		});
	-->
	</script>
	<div class="thumbnails">
		<div class="span4 thumbnail sb-pad">
			<form id="F" runat="server" action="/Pages/Usr/Fetch.aspx" class="sbForm">
				<fieldset id="F1">
				<legend>Sign in</legend>
				<br />
				<h5>Email</h5>
				<div><asp:TextBox ID="Eml" runat="server" MaxLength="127" CssClass="input-block-level"></asp:TextBox></div>
				<br />
				<h5>Password</h5>
				<div><asp:TextBox ID="Pwd" runat="server" MaxLength="127" TextMode="Password" CssClass="input-block-level"></asp:TextBox></div>
				<br /><br />
				<div><asp:Button ID="Sbmt" runat="server" Text="Sign in" onclick="Sbmt_Click" CssClass="btn btn-block btn-large btn-primary" /></div>
				</fieldset>
			</form>
			<hr />
			<div class="sb-mid">
				<p><a class="sb-txt-med" href="/Pages/Usr/ChngPwd.aspx">Forgot Password?</a></p>
				<p><a class="sb-txt-med" href="/Pages/Usr/AddNew.aspx">Don't have an account?</a></p>
				<p><a class="sb-txt-med" href="/Pages/Cmn/Spprt.aspx">Some other problem? Contact us</a></p>
			</div>
		</div>
		<div class="span7">
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="offset1 span2"><img src="/Base/Img/Usr/Fetch/work_hard.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Far and away the best prize that life has to offer is the chance to work hard at work worth doing.<br />
					<br />
					<em>- Theodore Roosevelt</em>
				</p>
			</div>
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="offset1 span2"><img src="/Base/Img/Usr/Fetch/innovation.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Innovation has nothing to do with how many R&D dollars you have. When Apple came up with the Mac, IBM was spending at least 100 times more on R&D. It's not about money. It's about the people you have, how you're led, and how much you get it.<br />
					<br />
					<em>- Steve Jobs</em>
				</p>
			</div>
		</div>
	</div>
</asp:Content>
