﻿<%@ Page Title="Reset Password - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="ChngPwd.aspx.cs" Inherits="Sammelana_NITR.Pages.Usr.ChngPwd" %>
<asp:Content ID="ChngPwd_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="ChngPwd_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<script type="text/javascript">
	<!--
		// Global variables
		var cp_acc_vld = false;

		$(document).ready(function () {
			// Ensure Form validation
			$("#<%= CP_Email.ClientID %>").focusout(function () { sbVldtFld("#<%= CP_Email.ClientID %>", "email", "", "#CPEmail1", "#CPEmail2"); });
			$("#<%= CP.ClientID %>").submit(function () {
				// Validate Email and Password from Server
				if (cp_acc_vld) return true;
				var email = sbVldtFld("#<%= CP_Email.ClientID %>", "email", "", "#CPEmail1", "#CPEmail2");
				if (email) {
					var v_eml = $("#<%= CP_Email.ClientID %>").val();
					$.post("/Pages/Users/ChangePassword.aspx", { Eml: v_eml }, function (resp) {
						if (resp != "") { $("#CPEmail3").show(vldtrUpdate); cp_acc_vld = false; }
						else { cp_acc_vld = true; $("#<%= CP_Sbmt.ClientID %>").click(); }
					});
				}
				return false;
			});
		});
	-->
	</script>
	<div class="thumbnails">
		<div class="span4 thumbnail sb-pad">
			<form id="CP" runat="server" action="/Pages/Users/ChangePassword.aspx" method="post">
				<fieldset>
				<legend>Change Password</legend>
				<br />
				<h5>Email</h5>
				<div><asp:TextBox ID="CP_Email" runat="server" MaxLength="127" autofocus="autofocus" CssClass="input-block-level"></asp:TextBox></div>
				<div id="CPEmail1" class="alert sb-hdn">No email-id entered|Please enter your email-id here</div>
				<div id="CPEmail2" class="alert sb-hdn">Invalid email-id entered|Please enter a valid email-id here</div>
				<div id="CPEmail3" class="alert sb-hdn">No such account exists|The email id you entered is not registered</div>
				<br /><br />
				<div><asp:Button ID="CP_Sbmt" runat="server" Text="Get Password Reset Link" onclick="CP_Sbmt_Click" CssClass="btn btn-block btn-large btn-primary" /></div>
				</fieldset>
			</form>
			<hr />
			<div class="sb-mid">
				<p><a href="/Pages/Users/ChangePassword.aspx">Forgot Password?</a></p>
				<p><a href="/Pages/Users/SignUp.aspx">Don't have an account?</a></p>
				<p><a href="/Pages/Mixed/ContactSupport.aspx">Some other problem? Contact us</a></p>
			</div>
		</div>
		<div class="span7">
			<h3 class="sb-mid sb-pad">Password reset process</h3>
			<br />
			<p>
				Please enter your email-id in the form below to recieve the password reset link. You will recieve furthur instructions to reset your account password in your email.
			</p>
		</div>
	</div>
</asp:Content>
