﻿<%@ Page Title="Sign Up - Sammelana" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="Sammelana_NITR.Pages.Usr.AddNew" %>
<asp:Content ID="AddNew_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="AddNew_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<script type="text/javascript">
	<!--
		// interactive Eml & Pwd check
		var oEml = "", oPwd = "", oVld = false;
		function ChkEmlPwd() {
			var eml = $("#<%= Eml.ClientID %>").val();
			var pwd = $("#<%= Pwd.ClientID %>").val();
			if (pwd.length == 0 || (oVld && oEml === eml && oPwd === pwd)) return true;
			oEml = eml; oPwd = pwd;
			$.post("/Pages/Usr/AddNew.aspx", { "Req": "sbChkEmail", "Eml": eml, "Pwd": pwd }, function (resp) {
				if (resp === "" || resp === null) { oVld = true; $("#<%= Sbmt.ClientID %>").click(); }
			});
			return false;
		}

		// Execute on Load
		$(document).ready(function () {
			// update the document
			sbForm.VldtrDir = "left";
			$("#<%= DoB.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, minDate: "-80Y", maxDate: "-10Y", yearRange: "c-80:c" });
			sbForm_AddVldtn({
				"Nm": "#<%= AN.ClientID %>",
				"Sbmt": { "Nm": "#<%= Sbmt.ClientID %>", "FtTxt": "Continue", "FtCls": "btn-primary", "LtTxt": "Sign Up", "LtCls": "btn-success" },
				"Prev": { "Nm": "#<%= Prev.ClientID %>"},
				"Blk": [
					{ "Nm": "#AN1", "Fld": [
							{ "Nm": "#<%= FtNm.ClientID %>", "Typ": "Nm", "MsgBlnk": "First Name not entered|Please enter your first name here", "MsgInv": "First Name is invalid|Your first name contains invalid characters"},
							{ "Nm": "#<%= MdNm.ClientID %>", "Typ": "Nm", "MsgInv": "Middle Name is invalid|" },
							{ "Nm": "#<%= LtNm.ClientID %>", "Typ": "Nm", "MsgBlnk": "Last Name not entered|Please enter your last name here", "MsgInv": "Last Name is invalid|Your last name contains invalid characters" },
							{ "Nm": "#<%= DoB.ClientID %>", "Typ": "Date", "MsgBlnk": "Date of Birth not entered|Please enter your date of birth", "MsgInv": "Date of Birth is invalid|Please enter your date of birth in dd/mm/yyyy format" },
							{ "Nm": "#<%= Gndr.ClientID %>", "Typ": "Gndr", "MsgBlnk": "Gender not entered|Please enter your gender" }
						]
					},
					{ "Nm": "#AN2", "Fld": [
							{ "Nm": "#<%= Eml.ClientID %>", "Typ": "Eml", "Fn": ChkEmlPwd, "MsgBlnk": "No email-id entered|Please enter your email-id here so that we can contact you for more information", "MsgInv": "Invalid email-id entered|Please enter a valid email-id here", "MsgFn": "Email already registered|This email-id has already been registered. If this is your email-id then, you should sign in." },
							{ "Nm": "#<%= Pwd.ClientID %>", "Typ": "Pwd", "Fn": ChkEmlPwd, "MsgBlnk": "No password entered|Please enter your new account password here", "MsgInv": "Password too small|Please enter a password of length 6 characters or more" },
							{ "Nm": "#<%= CPwd.ClientID %>", "Typ": "Mtch", "Typd": "#<%= Pwd.ClientID %>", "MsgBlnk": "Password not confirmed|Please confirm your account password here", "MsgInv": "Passwords dont match|Please enter the same password here as above" }
						]
					},
					{ "Nm": "#AN3", "Fld": [
							{ "Nm": "#<%= Pos.ClientID %>", "Typ": "Addr", "MsgBlnk": "No current position entered|Please enter your position held by you at your organization", "MsgInv": "Invalid current position entered|Your current position contains invalid characters" },
							{ "Nm": "#<%= Org.ClientID %>", "Typ": "Addr", "MsgBlnk": "No organization entered|Please enter name of the organization where you hold your office at the present", "MsgInv": "Invalid organization entered|Your organization name contains invalid characters" },
							{ "Nm": "#<%= WrkH.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid work history entered|Your work history contains invalid characters" }
						]
					},
					{ "Nm": "#AN4", "Fld": [
							{ "Nm": "#<%= Noff.ClientID %>", "Typ": "Phn", "MsgInv": "Invalid office number entered|Your office number contains invalid characters" },
							{ "Nm": "#<%= Nhom.ClientID %>", "Typ": "Phn", "MsgInv": "Invalid home number entered|Your home number contains invalid characters" },
							{ "Nm": "#<%= Nmob.ClientID %>", "Typ": "Phn", "MsgInv": "Invalid mobile number entered|Your mobile number contains invalid characters" },
							{ "Nm": "#<%= Nfax.ClientID %>", "Typ": "Phn", "MsgInv": "Invalid fax number entered|Your fax number contains invalid characters" }
						]
					},
					{ "Nm": "#AN5", "Fld": [
							{ "Nm": "#<%= AdAt.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid street address entered|Your street address contains invalid characters" },
							{ "Nm": "#<%= AdAr.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid area address entered|Your area address contains invalid characters" },
							{ "Nm": "#<%= AdCt.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid city entered|Your city contains invalid characters" },
							{ "Nm": "#<%= AdSt.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid state entered|Your state contains invalid characters" },
							{ "Nm": "#<%= AdCn.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid country entered|Your country contains invalid characters" },
							{ "Nm": "#<%= AdPn.ClientID %>", "Typ": "Addr", "MsgInv": "Invalid PIN code entered|Your PIN code contains invalid characters" },
						]
					}
				]
			});
		});
	-->
	</script>
	<div class="thumbnails">
		<div class="span7">
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="span2"><img src="/Base/Img/Usr/AddNew/call4papers.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					It is required to sign up here before submitting paper to a conference that is supported by Sammelana. After sign up process
					is complete, sign in to visit conference page at Sammelana, and submit your paper for the conference.<br />
				</p>
			</div>
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="span2"><img src="/Base/Img/Usr/AddNew/Peer-Reviewed-Papers.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Papers can be reviewed and marked by a reviewer after signing in. Each reviewer is usually appointed to review a given number
					of papers by the program chair. Reviewers are requested to consult the program chair regarding paper allocation to them.<br />
				</p>
			</div>
			<br />
			<br />
			<br />
			<div class="thumbnails sb-note">
				<div class="span2"><img src="/Base/Img/Usr/AddNew/Event management pic.jpg" alt="Simple" class="sb-box img-rounded" /></div>
				<p class="span4 sb-txt-med">
					Sammelana is a platform specially developed to ease the management of conference by providing intuitive ways to control and
					manage the activities of the conference. If you are a program chair, we hope you find managing your conference satisfying.<br />
				</p>
			</div>
		</div>
		<div class="span4 thumbnail sb-pad">
			<form id="AN" runat="server" method="post" action="/Pages/Usr/AddNew.aspx" class="sbForm">
				<fieldset id="AN1">
					<legend>Sign Up - Personal Information</legend>
					<br />
					<h5>First Name</h5>
					<div><asp:TextBox ID="FtNm" runat="server" CssClass="input-block-level" autofocus="autofocus"></asp:TextBox></div>
					<asp:Label ID="SUFtNm" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Middle Name</h5>
					<div><asp:TextBox ID="MdNm" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUMdNm" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Last Name</h5>
					<div><asp:TextBox ID="LtNm" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SULtNm" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Date of Birth (DD/MM/YYYY)</h5>
					<div><asp:TextBox ID="DoB" runat="server" CssClass="input-block-level" MaxLength="10"></asp:TextBox></div>
					<asp:Label ID="SUDoB" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Gender</h5>
					<div>
						<asp:RadioButtonList ID="Gndr" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="btn-group">
						<asp:ListItem class="btn btn-mini">Male</asp:ListItem>
						<asp:ListItem class="btn btn-mini">Female</asp:ListItem>
						</asp:RadioButtonList>
					</div>
					<asp:Label ID="SUGndr" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<fieldset id="AN2">
					<legend>Sign Up - Account Data</legend>
					<br />
					<h5>Email address</h5>
					<div><asp:TextBox ID="Eml" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUEml" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Password</h5>
					<div><asp:TextBox ID="Pwd" runat="server" CssClass="input-block-level" TextMode="Password"></asp:TextBox></div>
					<asp:Label ID="SUPwd" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Confirm Password</h5>
					<div><asp:TextBox ID="CPwd" runat="server" CssClass="input-block-level" TextMode="Password"></asp:TextBox></div>
					<asp:Label ID="SUCPwd" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Photo</h5>
					<div><asp:FileUpload ID="Pic" runat="server" Width="100%" CssClass="btn" /></div>
					<asp:Label ID="SUPic" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<fieldset id="AN3">
					<legend>Sign Up - Work Details</legend>
					<br />
					<h5>Current Position</h5>
					<div><asp:TextBox ID="Pos" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUPos" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Organization</h5>
					<div><asp:TextBox ID="Org" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUOrg" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Work History</h5>
					<div><asp:TextBox ID="WrkH" runat="server" TextMode="MultiLine" Height="100px" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUWrkH" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Curriculum Vitae</h5>
					<div><asp:FileUpload ID="CV" runat="server" Width="100%" CssClass="btn" /></div>
					<asp:Label ID="SUCV" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<fieldset id="AN4">
					<legend>Sign Up - Contact Information</legend>
					<br />
					<h5>Office Number</h5>
					<div><asp:TextBox ID="Noff" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUNoff" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Home Number</h5>
					<div><asp:TextBox ID="Nhom" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUNhom" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Mobile Number</h5>
					<div><asp:TextBox ID="Nmob" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUNmob" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Fax Number</h5>
					<div><asp:TextBox ID="Nfax" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUNfax" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<fieldset id="AN5">
					<legend>Sign Up - Communication Address</legend>
					<br />
					<h5>At</h5>
					<div><asp:TextBox ID="AdAt" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUAdAt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Area</h5>
					<div><asp:TextBox ID="AdAr" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUAdAr" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>City</h5>
					<div><asp:TextBox ID="AdCt" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUAdCt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>State</h5>
					<div><asp:TextBox ID="AdSt" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUAdSt" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>Country</h5>
					<div><asp:TextBox ID="AdCn" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUAdCn" runat="server" CssClass="alert sb-hdn"></asp:Label>
					<br />
					<h5>PIN Code</h5>
					<div><asp:TextBox ID="AdPn" runat="server" CssClass="input-block-level"></asp:TextBox></div>
					<asp:Label ID="SUAdPn" runat="server" CssClass="alert sb-hdn"></asp:Label>
				</fieldset>
				<br /><br />
				<table class="sb-full sb-mid sb-btn">
				<tr>
					<td><asp:Button ID="Prev" runat="server" Text="Back" CssClass="btn btn-block btn-large" /></td>
					<td><asp:Button ID="Sbmt" runat="server" Text="Continue" onclick="Sbmt_Click" CssClass="btn btn-block btn-large btn-primary" /></td>
				</tr>
				</table>
			</form>
		</div>
	</div>
</asp:Content>
