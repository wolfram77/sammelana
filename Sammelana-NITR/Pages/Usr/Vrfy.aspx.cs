﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Sammelana_NITR.Core;

namespace Sammelana_NITR.Pages.Usr
{
	// Global Definitions
	using sbKeyVals = Dictionary<string, object>;
	using sbKeyVal = KeyValuePair<string, object>;
	using sbKeys = Dictionary<string, object>.KeyCollection;
	using sbVals = Dictionary<string, object>.ValueCollection;

	public partial class Vrfy : System.Web.UI.Page
	{
		// Global Variables
		string wbMode, wbCode, rqEml, rqPwd;
		Guid wbAId;

		// Functions
		// get request data
		protected void Msg_GetQuery()
		{
			// Get Query Strings
			wbMode = Request.QueryString["md"];
			wbCode = Request.QueryString["cd"];
			wbAId = sbForm.GetGuid(wbCode);
			// Get JSON request data
			rqEml = Request["Eml"];
			rqPwd = Request["Pwd"];
		}
		// initialize page
		protected void InitPage()
		{
			EV_Dft.Visible = false;
			EV_VrSnt.Visible = false;
			EV_VrMl.Visible = false;
			EV_VrDn.Visible = false;
		}
		// execute user mail verification
		protected void UsrSgnUpCnf()
		{
			int sel;
			
			sbDb db = new sbDb();
			sel = sbUsr.AddCnf(db, wbAId);
			if (sel < 0) Response.Redirect("/Pages/Users/EmailVerify.aspx");
			else Response.Redirect("/Pages/Users/EmailVerify.aspx?md=vrdn");
			db.Disconnect();
		}

		// Event Handlers
		protected void Page_Load (object sender, EventArgs e)
		{
			Msg_GetQuery();
			InitPage();
			switch(wbMode)
			{
				case "vrsnt":
					// SignUp.aspx asking to display: Verification mail sent
					EV_VrSnt.Visible = true;
					break;
				case "vrml":
					// User clicked the verification link (provided in email)
					EV_VrMl.Visible = true;
					Response.Flush();
					UsrSgnUpCnf();
					break;
				case "vrdn":
					// Usr verification success
					EV_VrDn.Visible = true;
					break;
				default:
					// ERROR!!!
					EV_Dft.Visible = true;
					break;
			}
		}
	}
}
