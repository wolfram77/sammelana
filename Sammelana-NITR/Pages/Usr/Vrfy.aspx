﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Main.Master" AutoEventWireup="true" CodeBehind="Vrfy.aspx.cs" Inherits="Sammelana_NITR.Pages.Usr.Vrfy" %>
<asp:Content ID="Vrfy_Head" ContentPlaceHolderID="Main_Head" runat="server">
</asp:Content>
<asp:Content ID="Vrfy_Body" ContentPlaceHolderID="Main_Body" runat="server">
	<div>
	<form id="EV" runat="server" action="/Pages/Users/EmailVerify.aspx">
		<div id="EV_Dft" runat="server">
			<div class="sb-form">
				<h4>We have encountered some error</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				You have requested an action which we cannot perform. It could probably be due to a broken or tampered link which brought you here.
				</p>
			</div>
		</div>
		<div id="EV_VrSnt" runat="server">
			<div class="sb-form">
				<h4>Sign up - Verify Email</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				The sign up process for your account is almost complete. However we need to verify your email address for security purposes.
				A verification email has been sent to your e-mail address. Please verify it before you can use your account.
				</p>
				<p class="txt-hd" style="color: Gray;">
				It may take the verification mail about 1-2 minutes to reach your email address. Please be patient. Once your email has been
				verified, you can start by joining a conference as an author or a reviewer; or create a new conference as a program chairperson.
				</p>
			</div>
		</div>
		<div id="EV_VrMl" runat="server">
			<div class="sb-form">
				<h4>Sign up - Verifying Email</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				Please wait while we verify your email address ...
				</p>
			</div>
		</div>
		<div id="EV_VrDn" runat="server">
			<div class="sb-form">
				<h4>Sign up - Email Verification complete</h4>
				<br />
				<p class="txt-hd" style="color: Gray;">
				Your email address has been verified and the sign up process has compeleted. You can now login to your account by clicking
				<a href="/Pages/Users/SignIn.aspx">here</a>.
				</p>
			</div>
		</div>
	</form>
	</div>
</asp:Content>
